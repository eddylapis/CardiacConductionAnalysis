#pragma once

#define UNIT_INPUT_MAX  20            //定义最大输入节点的数量
#define NAME_LEN_MAX    40            //节点名字
#define UNIFORM_NUM_COUNT  200        //正态分布数组长度

//定义表盘刻度（每半小时一个刻度）
typedef enum
{
	A_R_BOTTOM = 0,//右房底
	A_8_0,        //右心房，右Kent束
	A_8_30,       //右心房，右房异位起搏点
	A_9_0,        //右心房
	A_9_30,       //右心房，Bachmann，窦房结
	A_10_0,       //右心房，Bachmann
	A_10_30,      //右心房，Bachmann
	A_11_0,       //左心房，Bachmann
	A_11_30,      //左心房
	A_0_0,        //左心房
	A_0_30,       //左心房
	A_1_0,        //左心房，左房异位起搏点
	A_L_BOTTOM,   //左房底，左Kent束
	ATRIUM_UNIT_COUNT
} AtriumIndex;

typedef enum
{
	V_7_30 = 0,   //右Kent束
	V_7_0,        //右心室
	V_6_30,       //右心室，右室异位起搏点
	V_6_0,        //右心室
	V_5_30,       //右心室
	V_5_0,        //右心室
	V_4_30,       //左心室
	V_4_0,        //左心室
	V_3_30,       //左心室
	V_3_0,        //左心室
	V_2_30,       //左心室
	V_2_0,        //左心室，左室异位起搏点
	V_1_30,       //左心室
	VENTRICLE_UNIT_COUNT//左Kent束
} VentricleIndex;

typedef enum
{
	BA_9_30 = 0,  //Bachmann束
	BA_10_0,
	BA_10_30,
	BA_11_0,
	BACHMANN_UNIT_COUNT
} BachmannIndex;

typedef enum
{
	RBB_7_30 = 0,   //Right Bundle Branch]
	RBB_7_0,
	RBB_6_30,
	RBB_6_0,
	RBB_5_30,
	RBB_5_0,
	RBB_ROOT,
	RBB_UNIT_COUNT  //右束支
} RBBIndex;

typedef enum
{
	LBB_ROOT = 0,   //Left Bundle Branch
	LBB_4_0,
	LBB_3_30,
	LBB_3_0,
	LBB_2_30,
	LBB_2_0,
	LBB_1_30,
	LBB_UNIT_COUNT  //左束支
} LBBIndex;


#define TERMINAL_STATUS_RESTING     0 //静息
#define TERMINAL_STATUS_POLARIZED   1 //极化状态

#define TERMINAL_OUTPUT_ACTIVE      1
#define TERMINAL_OUTPUT_INACTIVE    0
#define TERMINAL_OUTPUT_REFRACTORY  2

typedef struct __tag_TERMINAL_STATUS
{
	int    nStatus;                   //当前状态
	int    nStatusTimeCounter;        //当前状态时间计数器（单位mS）
	int    nTerminalOutput;           //输出状态

	int*   aryInputSource[UNIT_INPUT_MAX];  //信号传入点
	float  constActiveRatio;          //动作占空比
	float  constRefractoryRatio;      //不应期占空比
	int    constActivePeriod;         //动作时间（单位mS）
	int    constRefractoryPeriod;     //有效不应期（单位mS）

	int    constPinX;    //在示意图上的位置信息
	int    constPinY;

	TCHAR  ts_name[NAME_LEN_MAX];
} TERMINAL_STATUS;

typedef struct __tag_SPONTANEOUS_UNIT //自律单元
{
	TERMINAL_STATUS ts;

	float  fBaseFreqency;             //基本频率（次每分钟）
	float  fGaussianVariance;         //高斯分布的方差σ
	int    nInactiveTimeDelay;        //不响应时间
	int    constSuppressedTimeDelay;  //外来刺激抑制时间（单位mS）

	BOOL   bOutputBlock;              //是否存在输出阻滞
	TCHAR  name[NAME_LEN_MAX];
	int    m_aryUniformNum[UNIFORM_NUM_COUNT];
	int    m_nUniformIndex;
} SPONTANEOUS_UNIT;

typedef struct __tag_CONDUCTION_UNIT  //传导单元
{
	TERMINAL_STATUS ts1;
	TERMINAL_STATUS ts2; 

	//暂时不启用韦金斯基现象
	//int    nKickCounter1;             //端点1的踹门计数器
	//int    nKickCounter2;             //端点2的踹门计数器
	//int    constKickCount;            //踹门次数设定（韦金斯基现象）
	//BOOL   constEnableWedensky1;      //端点1启用韦金斯基现象
	//BOOL   constEnableWedensky2;      //端点2启用韦金斯基现象

	int    nFirstActive;              //首先被激活的是哪一端
	int    nConductionCounter;        //传导计数器
	int    constConductionTime;       //端到端的传导时间（单位mS）

	BOOL   bCompleteBlock;            //是否完全阻滞
	BOOL   b1to2Block;                //端1到端2传导阻滞
	BOOL   b2to1Block;                //端2到端1传导阻滞
	TCHAR  name[NAME_LEN_MAX];
} CONDUCTION_UNIT;

typedef struct __tag_CARDIAC_MODEL
{
	SPONTANEOUS_UNIT suSinusNode;     //窦房结（心房肌A_9_30）
	SPONTANEOUS_UNIT suEctopicPacemakerOnRA; //右心房的异位起搏点（心房肌A_8_30）
	SPONTANEOUS_UNIT suEctopicPacemakerOnLA; //左心房的异位起搏点（心房肌A_1_0）
	SPONTANEOUS_UNIT suEctopicPacemakerOnRV; //右心室的异位起搏点（心室肌V_7_0）
	SPONTANEOUS_UNIT suEctopicPacemakerOnLV; //左心室的异位起搏点（心室肌V_2_30）
	SPONTANEOUS_UNIT suJunction;      //房室交界区自律（房室结下端）
	
	CONDUCTION_UNIT  cuS_A;           //房间束（窦房结到房室结上端）
	CONDUCTION_UNIT  cuJames;         //跨越房室结的James束（房室结上端到房室结下端）
	CONDUCTION_UNIT  cuAVFast;        //房室结快路径（房室结上端到房室结下端）
	CONDUCTION_UNIT  cuAVSlow;        //房室结慢路径（房室结上端到房室结下端）
	CONDUCTION_UNIT  cuKentLeft;      //左Kent束（心房肌A_8_0到心室肌V_8_0）
	CONDUCTION_UNIT  cuKentRight;     //右Kent束（心房肌A_1_30到心室肌V_1_30）
	CONDUCTION_UNIT  cuHisBundle;     //希氏束（房室结下端到希氏束下端）

	CONDUCTION_UNIT  aryBachmann[BACHMANN_UNIT_COUNT]; //左房间束
	CONDUCTION_UNIT  aryRBB[RBB_UNIT_COUNT]; //右束支
	CONDUCTION_UNIT  aryLBB[LBB_UNIT_COUNT]; //左束支

	CONDUCTION_UNIT  cuIAS;           //房间隔
	CONDUCTION_UNIT  aryAtrium[ATRIUM_UNIT_COUNT];      //心房肌传导环
	CONDUCTION_UNIT  cuIVS;           //室间隔（紧贴左束支）
	CONDUCTION_UNIT  aryVentricle[VENTRICLE_UNIT_COUNT];//心室肌传导环

} CARDIAC_MODEL;

class CardiacConfig
{
private:
	CARDIAC_MODEL  m_Model;

	SPONTANEOUS_UNIT* m_suGroup[6];
	CONDUCTION_UNIT*  m_cuGroup[9
		+ BACHMANN_UNIT_COUNT
		+ RBB_UNIT_COUNT + LBB_UNIT_COUNT
		+ ATRIUM_UNIT_COUNT + VENTRICLE_UNIT_COUNT];
	//CONDUCTION_UNIT*  m_cuGroup[10 + VENTRICLE_UNIT_COUNT];
	//SPONTANEOUS_UNIT* m_suGroup[2];
	//CONDUCTION_UNIT*  m_cuGroup[2];

private:
	void InitGroup();
	void InitPinPos();
	void InitUnitName();
	void InitConnection();
	void Reset();
	void GetTsPos(int* pTerminalOutput, CString& sName);
	void SetTsName(TERMINAL_STATUS* pts, TCHAR* node_name, TCHAR* sub_name);

	BOOL AddConnection(TERMINAL_STATUS* pts, int* input);
	BOOL JoinSingleChain(CONDUCTION_UNIT* pcu, int size);
	BOOL JoinDuplexChain(CONDUCTION_UNIT* pcu1, CONDUCTION_UNIT* pcu2, int size);

public:
	CardiacConfig(void);
	CardiacConfig(CardiacConfig* pCfg);
	virtual ~CardiacConfig(void);
	void InitNormal();
	void InitUniform();
	void Click_1ms();
	void SpontaneousUnitProc(SPONTANEOUS_UNIT* psu);
	void CondictionUnitProc(CONDUCTION_UNIT* pcu);
	BOOL Save(LPCTSTR lpszPathName);
	BOOL Load(LPCTSTR lpszPathName);

	inline CARDIAC_MODEL* GetModel() {
		return &m_Model;
	}
	inline void CopyTo(CARDIAC_MODEL* pTarget) {
		memcpy(pTarget, &m_Model, sizeof(m_Model));
	}
	inline void CopyFrom(CARDIAC_MODEL* pOrign) {
		memcpy(&m_Model, pOrign, sizeof(m_Model));
	}
	inline SPONTANEOUS_UNIT** GetSuGroup(int* size) {
		*size = sizeof(m_suGroup) / sizeof(m_suGroup[0]);
		return m_suGroup;
	}
	CONDUCTION_UNIT**  GetCuGroup(int* size) {
		*size = sizeof(m_cuGroup) / sizeof(m_cuGroup[0]);
		return m_cuGroup;
	}

	static CString GetUniString(SPONTANEOUS_UNIT* psu) {

		CString sRet = _T("");
		int k;
		for (k=0; k<UNIFORM_NUM_COUNT; k++) {
			CString s;
			s.Format(_T("%d "), psu->m_aryUniformNum[k]);
			sRet += s;
		}
		return sRet;
	}

	static int GetBeatGap(SPONTANEOUS_UNIT* psu) {

		int i = psu->m_nUniformIndex;
		psu->m_nUniformIndex++;
		if (psu->m_nUniformIndex == UNIFORM_NUM_COUNT) {
			psu->m_nUniformIndex = 0;
		}
		return psu->m_aryUniformNum[i];
	}
};

