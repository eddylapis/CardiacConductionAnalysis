
// CCAView.h : CCAView 类的接口
//

#pragma once

typedef struct __tag_PIN_POS
{
	int x;
	int y;
	int state;
} PIN_POS;

class CSmartDC;
class CCAView : public CScrollView
{
private:
	BOOL m_b1msPerPixel;

	CDC* m_pdcBackground;
	CDC* m_pdcSinusNode;     //窦房结（心房肌A_9_30）
	CDC* m_pdcEctopicPacemakerOnRA; //右心房的异位起搏点（心房肌A_8_30）
	CDC* m_pdcEctopicPacemakerOnLA; //左心房的异位起搏点（心房肌A_1_0）
	CDC* m_pdcEctopicPacemakerOnRV; //右心室的异位起搏点（心室肌V_7_0）
	CDC* m_pdcEctopicPacemakerOnLV; //左心室的异位起搏点（心室肌V_2_30）
	CDC* m_pdcJunction;      //房室交界区自律（房室结下端）
	
	CDC* m_pdcS_A;           //房间束（窦房结到房室结上端）
	CDC* m_pdcBachmann;      //左房间束（窦房结到心房肌A_0_0）
	CDC* m_pdcJames;         //跨越房室结的James束（房室结上端到房室结下端）
	CDC* m_pdcAVFast;        //房室结快路径（房室结上端到房室结下端）
	CDC* m_pdcAVSlow;        //房室结慢路径（房室结上端到房室结下端）
	CDC* m_pdcKentLeft;      //左Kent束（心房肌A_8_0到心室肌V_8_0）
	CDC* m_pdcKentRight;     //右Kent束（心房肌A_1_30到心室肌V_1_30）
	CDC* m_pdcHisBundle;     //希氏束（房室结下端到希氏束下端）
	CDC* m_pdcRBB;           //右束支
	CDC* m_pdcLBB;           //左束支

	CDC* m_pdcIAS;           //房间隔
	CDC* m_pdcIVS;           //室间隔
	CDC* m_pdcAtrium;        //心房肌传导环
	CDC* m_pdcVentricle;     //心室肌传导环

	CSmartDC* m_pMemDC;
	CSmartDC* m_pBarChartDC;
	CSmartDC* m_pLocationDC;
	CSmartDC* m_pSketchImageDC;
	int m_nLocationBmpW;
	int m_nLocationBmpH;

	int m_nMousePosX;
	int m_nMousePosY;
	int m_nMouseDown;

	PIN_POS** m_pBarChartBuffer;
	int m_nBarChartRow;

protected: // 仅从序列化创建
	CCAView();
	DECLARE_DYNCREATE(CCAView)

// 特性
public:
	CCADoc* GetDocument() const;

// 操作
private:
	void InitUnitDC(CDC* pDC);
	void ReleaseBarChartBuffer();
	void InitSketchConnection(CCADoc* pDoc);
	BOOL GetTsPos(int* pTerminalOutput, int* x, int* y, int* myTerminal);
	void DrawConnectionLine(int* pInput, int x1, int y1, int* myTerminal);
	void DrawSuBlockSign(CDC* pdc, SPONTANEOUS_UNIT* psu);
	void DrawCuBlockSign(CDC* pdc, CONDUCTION_UNIT* pcu);

public:
	void RenewBarChart();

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CCAView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	void DrawAll(CCADoc* pDoc, CDC& dc);
	void DrawRuler(CDC& dc);
	void DrawHeadline(CCADoc* pDoc, CDC& dc);
	void DrawActiveBar(CCADoc* pDoc, CDC& dc);

public:
	void DrawSketch(CDC* pdc, CARDIAC_MODEL* pModel,
		BOOL bSinusNode,
		BOOL bPacemakerOnRA,
		BOOL bPacemakerOnLA,
		BOOL bPacemakerOnRV,
		BOOL bPacemakerOnLV,
		BOOL bJunction,
		BOOL bS_A,
		BOOL bBachmann,
		BOOL bJames,
		BOOL bAVFast,
		BOOL bAVSlow,
		BOOL bKentLeft,
		BOOL bKentRight,
		BOOL bHisBundle,
		BOOL bRBB,
		BOOL bLBB,
		BOOL bIAS,
		BOOL bIVS,
		BOOL bAtrium,
		BOOL bVentricule);

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:

	virtual void OnInitialUpdate();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void On1msPerPixel();
	afx_msg void On5msPerPixel();
	afx_msg void OnEditAttr();
	afx_msg void OnExportSequence();
};

#ifndef _DEBUG  // CCAView.cpp 中的调试版本
inline CCADoc* CCAView::GetDocument() const
   { return reinterpret_cast<CCADoc*>(m_pDocument); }
#endif

