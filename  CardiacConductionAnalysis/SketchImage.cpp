#include "stdafx.h"
#include "SmartDC.h"
#include "SketchImage.h"

SketchImage::SketchImage()
	: m_pdcSketch(NULL)
{
}

SketchImage::~SketchImage(void)
{
}

void SketchImage::SetSketchDC(CSmartDC* pdc, int w, int h)
{
	m_pdcSketch = pdc;
	m_w = w;
	m_h = h;
}

void SketchImage::PreSubclassWindow()
{
	DWORD dwStyle = GetStyle();
	SetWindowLong(GetSafeHwnd(),GWL_STYLE,dwStyle | SS_OWNERDRAW);
	CStatic::PreSubclassWindow();
}

void SketchImage::DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/)
{
	if (! m_pdcSketch) {
		return;
	}

    CDC *pdc = GetWindowDC();
	pdc->BitBlt(0, 0, m_w, m_h,
		m_pdcSketch, 0, 0, SRCCOPY);
    ReleaseDC(pdc);
}
