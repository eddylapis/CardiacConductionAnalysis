#include "stdafx.h"
#include "CardiacConfig.h"
#include <vector>
#include <iostream>
#include <random>
#include <ctime>

static TCHAR* _aryAtriumLabel[ATRIUM_UNIT_COUNT] =
{
	_T("右房底"),
	_T("房8:00"),
	_T("房8:30"),
	_T("房9:00"),
	_T("房9:30"),
	_T("房10:00"),
	_T("房10:30"),
	_T("房11:00"),
	_T("房11:30"),
	_T("房0:00"),
	_T("房0:30"),
	_T("房1:00"),
	_T("左房底")
};

static TCHAR* _aryVentricleLabel[VENTRICLE_UNIT_COUNT] =
{
	_T("室8:00"),
	_T("室7:30"),
	_T("室7:00"),
	_T("室6:30"),
	_T("室6:00"),
	_T("室5:30"),
	_T("室5:00"),
	_T("室4:30"),
	_T("室4:00"),
	_T("室3:30"),
	_T("室3:00"),
	_T("室3:30"),
	_T("室2:00")
};

static TCHAR* _aryBachmannLabel[VENTRICLE_UNIT_COUNT] =
{
	_T("BA_9:30"),
	_T("BA_10:00"),
	_T("BA_10:30"),
	_T("BA_11:00")
};

static TCHAR* _aryRBBLabel[VENTRICLE_UNIT_COUNT] =
{
	_T("RBB_7:30"),
	_T("RBB_7:00"),
	_T("RBB_6:30"),
	_T("RBB_6:00"),
	_T("RBB_5:30"),
	_T("RBB_5:00"),
	_T("RBB_ROOT")
};

static TCHAR* _aryLBBLabel[VENTRICLE_UNIT_COUNT] =
{
	_T("LBB_ROOT"),
	_T("LBB_4:00"),
	_T("LBB_3:30"),
	_T("LBB_3:00"),
	_T("LBB_2:30"),
	_T("LBB_2:00"),
	_T("LBB_1:30")
};

CardiacConfig::CardiacConfig(void)
{
	ZeroMemory(&m_Model, sizeof(m_Model));
	InitGroup();
	InitPinPos();
	InitUnitName();
	InitConnection(); //初始化各个节点之间的连接关系
}

CardiacConfig::CardiacConfig(CardiacConfig* pCfg)
{
	CopyFrom(pCfg->GetModel());//先把配置信息复制一份
	InitGroup();
	InitPinPos();
	InitUnitName();
	InitConnection();
	InitUniform();//初始化正态分布随机数数组
}

CardiacConfig::~CardiacConfig(void)
{
}

void CardiacConfig::InitGroup()
{
	//m_suGroup[0] = &m_Model.suSinusNode;
	//m_suGroup[1] = &m_Model.suJunction;
	//m_cuGroup[0] = &m_Model.cuBachmann;
	//m_cuGroup[1] = &m_Model.cuS_A;
	//return;


	int i = 0;
	m_suGroup[i++] = &m_Model.suSinusNode;
	m_suGroup[i++] = &m_Model.suEctopicPacemakerOnRA;
	m_suGroup[i++] = &m_Model.suEctopicPacemakerOnLA;
	m_suGroup[i++] = &m_Model.suEctopicPacemakerOnRV;
	m_suGroup[i++] = &m_Model.suEctopicPacemakerOnLV;
	m_suGroup[i++] = &m_Model.suJunction;
	i = 0;
	m_cuGroup[i++] = &m_Model.cuS_A;
	m_cuGroup[i++] = &m_Model.cuJames;
	m_cuGroup[i++] = &m_Model.cuAVFast;
	m_cuGroup[i++] = &m_Model.cuAVSlow;
	m_cuGroup[i++] = &m_Model.cuKentLeft;
	m_cuGroup[i++] = &m_Model.cuKentRight;
	m_cuGroup[i++] = &m_Model.cuHisBundle;

	int j, count;
	count = sizeof(m_Model.aryBachmann) / sizeof(m_Model.aryBachmann[0]);
	for (j=0; j<count; j++) {
		m_cuGroup[i++] = &m_Model.aryBachmann[j];
	}
	count = sizeof(m_Model.aryRBB) / sizeof(m_Model.aryRBB[0]);
	for (j=0; j<count; j++) {
		m_cuGroup[i++] = &m_Model.aryRBB[j];
	}
	count = sizeof(m_Model.aryLBB) / sizeof(m_Model.aryLBB[0]);
	for (j=0; j<count; j++) {
		m_cuGroup[i++] = &m_Model.aryLBB[j];
	}

	count = sizeof(m_Model.aryAtrium) / sizeof(m_Model.aryAtrium[0]);
	for (j=0; j<6; j++) {
		m_cuGroup[i++] = &m_Model.aryAtrium[j];
	}
	m_cuGroup[i++] = &m_Model.cuIAS;
	for (j=6; j<count; j++) {
		m_cuGroup[i++] = &m_Model.aryAtrium[j];
	}

	count = sizeof(m_Model.aryVentricle) / sizeof(m_Model.aryVentricle[0]);
	for (j=0; j<8; j++) {
		m_cuGroup[i++] = &m_Model.aryVentricle[j];
	}
	m_cuGroup[i++] = &m_Model.cuIVS;
	for (j=8; j<count; j++) {
		m_cuGroup[i++] = &m_Model.aryVentricle[j];
	}
}

BOOL CardiacConfig::AddConnection(TERMINAL_STATUS* pts, int* input)
{
	int i;
	for (i=0; i<UNIT_INPUT_MAX; i++) {
		if (input == pts->aryInputSource[i]) {
			//说明该节点已经添加过了
			return TRUE;
		}
		if (0 == pts->aryInputSource[i]) {
			pts->aryInputSource[i] = input;
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CardiacConfig::JoinSingleChain(CONDUCTION_UNIT* pcu, int size)
{
	int i;
	int count = size - 1;
	for (i=0; i<count; i++) {
		if (! AddConnection(&pcu[i].ts2, &pcu[i+1].ts1.nTerminalOutput)) {
			return FALSE;
		}
		if (! AddConnection(&pcu[i+1].ts1, &pcu[i].ts2.nTerminalOutput)) {
			return FALSE;
		}
	}
	return TRUE;
}

BOOL CardiacConfig::JoinDuplexChain(CONDUCTION_UNIT* pcu1, CONDUCTION_UNIT* pcu2, int size)
{
	int i;
	int count = size - 1;

	if (! AddConnection(&pcu1[0].ts1, &pcu2[0].ts1.nTerminalOutput)) {
		return FALSE;
	}
	if (! AddConnection(&pcu2[0].ts1, &pcu1[0].ts1.nTerminalOutput)) {
		return FALSE;
	}

	for (i=0; i<count; i++) {
		if (! AddConnection(&pcu1[i].ts2, &pcu1[i+1].ts1.nTerminalOutput)) {
			return FALSE;
		}
		if (! AddConnection(&pcu1[i+1].ts1, &pcu1[i].ts2.nTerminalOutput)) {
			return FALSE;
		}

		if (! AddConnection(&pcu2[i].ts2, &pcu2[i+1].ts1.nTerminalOutput)) {
			return FALSE;
		}
		if (! AddConnection(&pcu2[i+1].ts1, &pcu2[i].ts2.nTerminalOutput)) {
			return FALSE;
		}

		if (! AddConnection(&pcu1[i].ts2, &pcu2[i].ts2.nTerminalOutput)) {
			return FALSE;
		}
		if (! AddConnection(&pcu2[i].ts2, &pcu1[i].ts2.nTerminalOutput)) {
			return FALSE;
		}

		if (! AddConnection(&pcu1[i+1].ts1, &pcu2[i+1].ts1.nTerminalOutput)) {
			return FALSE;
		}
		if (! AddConnection(&pcu2[i+1].ts1, &pcu1[i+1].ts1.nTerminalOutput)) {
			return FALSE;
		}

		if (! AddConnection(&pcu1[i].ts2, &pcu2[i+1].ts1.nTerminalOutput)) {
			return FALSE;
		}
		if (! AddConnection(&pcu2[i+1].ts1, &pcu1[i].ts2.nTerminalOutput)) {
			return FALSE;
		}

		if (! AddConnection(&pcu2[i].ts2, &pcu1[i+1].ts1.nTerminalOutput)) {
			return FALSE;
		}
		if (! AddConnection(&pcu1[i+1].ts1, &pcu2[i].ts2.nTerminalOutput)) {
			return FALSE;
		}
	}

	if (! AddConnection(&pcu1[count].ts2, &pcu2[count].ts2.nTerminalOutput)) {
		return FALSE;
	}
	if (! AddConnection(&pcu2[count].ts2, &pcu1[count].ts2.nTerminalOutput)) {
		return FALSE;
	}
	return TRUE;
}

void CardiacConfig::InitPinPos()
{
	m_Model.suSinusNode.ts.constPinX = 9;
	m_Model.suSinusNode.ts.constPinY = 81;

	m_Model.suEctopicPacemakerOnRA.ts.constPinX = 15;
	m_Model.suEctopicPacemakerOnRA.ts.constPinY = 143;

	m_Model.suEctopicPacemakerOnLA.ts.constPinX = 187;
	m_Model.suEctopicPacemakerOnLA.ts.constPinY = 11;

	m_Model.suEctopicPacemakerOnRV.ts.constPinX = 69;
	m_Model.suEctopicPacemakerOnRV.ts.constPinY = 215;

	m_Model.suEctopicPacemakerOnLV.ts.constPinX = 242;
	m_Model.suEctopicPacemakerOnLV.ts.constPinY = 81;

	m_Model.suJunction.ts.constPinX = 128;
	m_Model.suJunction.ts.constPinY = 113;

	m_Model.aryBachmann[BA_9_30].ts1.constPinX = 23;
	m_Model.aryBachmann[BA_9_30].ts1.constPinY = 81;
	m_Model.aryBachmann[BA_9_30].ts2.constPinX = 31;
	m_Model.aryBachmann[BA_9_30].ts2.constPinY = 62;

	m_Model.aryBachmann[BA_10_0].ts1.constPinX = 35;
	m_Model.aryBachmann[BA_10_0].ts1.constPinY = 55;
	m_Model.aryBachmann[BA_10_0].ts2.constPinX = 48;
	m_Model.aryBachmann[BA_10_0].ts2.constPinY = 38;

	m_Model.aryBachmann[BA_10_30].ts1.constPinX = 53;
	m_Model.aryBachmann[BA_10_30].ts1.constPinY = 32;
	m_Model.aryBachmann[BA_10_30].ts2.constPinX = 70;
	m_Model.aryBachmann[BA_10_30].ts2.constPinY = 20;

	m_Model.aryBachmann[BA_11_0].ts1.constPinX = 77;
	m_Model.aryBachmann[BA_11_0].ts1.constPinY = 16;
	m_Model.aryBachmann[BA_11_0].ts2.constPinX = 95;
	m_Model.aryBachmann[BA_11_0].ts2.constPinY = 8;

	m_Model.cuS_A.ts1.constPinX = 39;
	m_Model.cuS_A.ts1.constPinY = 88;
	m_Model.cuS_A.ts2.constPinX = 61;
	m_Model.cuS_A.ts2.constPinY = 95;

	m_Model.cuJames.ts1.constPinX = 87;
	m_Model.cuJames.ts1.constPinY = 109;
	m_Model.cuJames.ts2.constPinX = 147;
	m_Model.cuJames.ts2.constPinY = 150;

	m_Model.cuAVFast.ts1.constPinX = 105;
	m_Model.cuAVFast.ts1.constPinY = 113;
	m_Model.cuAVFast.ts2.constPinX = 138;
	m_Model.cuAVFast.ts2.constPinY = 135;

	m_Model.cuAVSlow.ts1.constPinX = 107;
	m_Model.cuAVSlow.ts1.constPinY = 102;
	m_Model.cuAVSlow.ts2.constPinX = 147;
	m_Model.cuAVSlow.ts2.constPinY = 128;

	m_Model.cuKentLeft.ts1.constPinX = 200;
	m_Model.cuKentLeft.ts1.constPinY = 25;
	m_Model.cuKentLeft.ts2.constPinX = 217;
	m_Model.cuKentLeft.ts2.constPinY = 43;

	m_Model.cuKentRight.ts1.constPinX = 24;
	m_Model.cuKentRight.ts1.constPinY = 160;
	m_Model.cuKentRight.ts2.constPinX = 37;
	m_Model.cuKentRight.ts2.constPinY = 181;

	m_Model.cuHisBundle.ts1.constPinX = 156;
	m_Model.cuHisBundle.ts1.constPinY = 150;
	m_Model.cuHisBundle.ts2.constPinX = 164;
	m_Model.cuHisBundle.ts2.constPinY = 160;

	m_Model.aryRBB[RBB_7_30].ts1.constPinX = 51;
	m_Model.aryRBB[RBB_7_30].ts1.constPinY = 165;
	m_Model.aryRBB[RBB_7_30].ts2.constPinX = 60;
	m_Model.aryRBB[RBB_7_30].ts2.constPinY = 176;

	m_Model.aryRBB[RBB_7_0].ts1.constPinX = 66;
	m_Model.aryRBB[RBB_7_0].ts1.constPinY = 182;
	m_Model.aryRBB[RBB_7_0].ts2.constPinX = 78;
	m_Model.aryRBB[RBB_7_0].ts2.constPinY = 191;

	m_Model.aryRBB[RBB_6_30].ts1.constPinX = 85;
	m_Model.aryRBB[RBB_6_30].ts1.constPinY = 196;
	m_Model.aryRBB[RBB_6_30].ts2.constPinX = 100;
	m_Model.aryRBB[RBB_6_30].ts2.constPinY = 202;

	m_Model.aryRBB[RBB_6_0].ts1.constPinX = 107;
	m_Model.aryRBB[RBB_6_0].ts1.constPinY = 204;
	m_Model.aryRBB[RBB_6_0].ts2.constPinX = 124;
	m_Model.aryRBB[RBB_6_0].ts2.constPinY = 206;

	m_Model.aryRBB[RBB_5_30].ts1.constPinX = 132;
	m_Model.aryRBB[RBB_5_30].ts1.constPinY = 206;
	m_Model.aryRBB[RBB_5_30].ts2.constPinX = 149;
	m_Model.aryRBB[RBB_5_30].ts2.constPinY = 204;

	m_Model.aryRBB[RBB_5_0].ts1.constPinX = 156;
	m_Model.aryRBB[RBB_5_0].ts1.constPinY = 202;
	m_Model.aryRBB[RBB_5_0].ts2.constPinX = 171;
	m_Model.aryRBB[RBB_5_0].ts2.constPinY = 196;

	m_Model.aryRBB[RBB_ROOT].ts1.constPinX = 170;
	m_Model.aryRBB[RBB_ROOT].ts1.constPinY = 186;
	m_Model.aryRBB[RBB_ROOT].ts2.constPinX = 162;
	m_Model.aryRBB[RBB_ROOT].ts2.constPinY = 171;

	m_Model.aryLBB[LBB_ROOT].ts1.constPinX = 177;
	m_Model.aryLBB[LBB_ROOT].ts1.constPinY = 161;
	m_Model.aryLBB[LBB_ROOT].ts2.constPinX = 187;
	m_Model.aryLBB[LBB_ROOT].ts2.constPinY = 173;

	m_Model.aryLBB[LBB_4_0].ts1.constPinX = 197;
	m_Model.aryLBB[LBB_4_0].ts1.constPinY = 175;
	m_Model.aryLBB[LBB_4_0].ts2.constPinX = 206;
	m_Model.aryLBB[LBB_4_0].ts2.constPinY = 163;

	m_Model.aryLBB[LBB_3_30].ts1.constPinX = 211;
	m_Model.aryLBB[LBB_3_30].ts1.constPinY = 155;
	m_Model.aryLBB[LBB_3_30].ts2.constPinX = 216;
	m_Model.aryLBB[LBB_3_30].ts2.constPinY = 141;

	m_Model.aryLBB[LBB_3_0].ts1.constPinX = 219;
	m_Model.aryLBB[LBB_3_0].ts1.constPinY = 133;
	m_Model.aryLBB[LBB_3_0].ts2.constPinX = 221;
	m_Model.aryLBB[LBB_3_0].ts2.constPinY = 118;

	m_Model.aryLBB[LBB_2_30].ts1.constPinX = 220;
	m_Model.aryLBB[LBB_2_30].ts1.constPinY = 109;
	m_Model.aryLBB[LBB_2_30].ts2.constPinX = 219;
	m_Model.aryLBB[LBB_2_30].ts2.constPinY = 94;

	m_Model.aryLBB[LBB_2_0].ts1.constPinX = 216;
	m_Model.aryLBB[LBB_2_0].ts1.constPinY = 85;
	m_Model.aryLBB[LBB_2_0].ts2.constPinX = 211;
	m_Model.aryLBB[LBB_2_0].ts2.constPinY = 71;

	m_Model.aryLBB[LBB_1_30].ts1.constPinX = 206;
	m_Model.aryLBB[LBB_1_30].ts1.constPinY = 63;
	m_Model.aryLBB[LBB_1_30].ts2.constPinX = 198;
	m_Model.aryLBB[LBB_1_30].ts2.constPinY = 53;

	m_Model.cuIAS.ts1.constPinX = 64;
	m_Model.cuIAS.ts1.constPinY = 50;
	m_Model.cuIAS.ts2.constPinX = 77;
	m_Model.cuIAS.ts2.constPinY = 63;

	m_Model.cuIVS.ts1.constPinX = 172;
	m_Model.cuIVS.ts1.constPinY = 167;
	m_Model.cuIVS.ts2.constPinX = 188;
	m_Model.cuIVS.ts2.constPinY = 183;

	m_Model.aryAtrium[A_R_BOTTOM].ts1.constPinX = 63;
	m_Model.aryAtrium[A_R_BOTTOM].ts1.constPinY = 141;
	m_Model.aryAtrium[A_R_BOTTOM].ts2.constPinX = 45;
	m_Model.aryAtrium[A_R_BOTTOM].ts2.constPinY = 151;

	m_Model.aryAtrium[A_8_0].ts1.constPinX = 36;
	m_Model.aryAtrium[A_8_0].ts1.constPinY = 158;
	m_Model.aryAtrium[A_8_0].ts2.constPinX = 31;
	m_Model.aryAtrium[A_8_0].ts2.constPinY = 145;

	m_Model.aryAtrium[A_8_30].ts1.constPinX = 28;
	m_Model.aryAtrium[A_8_30].ts1.constPinY = 135;
	m_Model.aryAtrium[A_8_30].ts2.constPinX = 26;
	m_Model.aryAtrium[A_8_30].ts2.constPinY = 119;

	m_Model.aryAtrium[A_9_0].ts1.constPinX = 26;
	m_Model.aryAtrium[A_9_0].ts1.constPinY = 109;
	m_Model.aryAtrium[A_9_0].ts2.constPinX = 28;
	m_Model.aryAtrium[A_9_0].ts2.constPinY = 91;

	m_Model.aryAtrium[A_9_30].ts1.constPinX = 30;
	m_Model.aryAtrium[A_9_30].ts1.constPinY = 82;
	m_Model.aryAtrium[A_9_30].ts2.constPinX = 37;
	m_Model.aryAtrium[A_9_30].ts2.constPinY = 66;

	m_Model.aryAtrium[A_10_0].ts1.constPinX = 42;
	m_Model.aryAtrium[A_10_0].ts1.constPinY = 59;
	m_Model.aryAtrium[A_10_0].ts2.constPinX = 52;
	m_Model.aryAtrium[A_10_0].ts2.constPinY = 44;

	m_Model.aryAtrium[A_10_30].ts1.constPinX = 59;
	m_Model.aryAtrium[A_10_30].ts1.constPinY = 38;
	m_Model.aryAtrium[A_10_30].ts2.constPinX = 73;
	m_Model.aryAtrium[A_10_30].ts2.constPinY = 27;

	m_Model.aryAtrium[A_11_0].ts1.constPinX = 81;
	m_Model.aryAtrium[A_11_0].ts1.constPinY = 22;
	m_Model.aryAtrium[A_11_0].ts2.constPinX = 97;
	m_Model.aryAtrium[A_11_0].ts2.constPinY = 15;

	m_Model.aryAtrium[A_11_30].ts1.constPinX = 105;
	m_Model.aryAtrium[A_11_30].ts1.constPinY = 13;
	m_Model.aryAtrium[A_11_30].ts2.constPinX = 123;
	m_Model.aryAtrium[A_11_30].ts2.constPinY = 10;

	m_Model.aryAtrium[A_0_0].ts1.constPinX = 132;
	m_Model.aryAtrium[A_0_0].ts1.constPinY = 10;
	m_Model.aryAtrium[A_0_0].ts2.constPinX = 150;
	m_Model.aryAtrium[A_0_0].ts2.constPinY = 13;

	m_Model.aryAtrium[A_0_30].ts1.constPinX = 159;
	m_Model.aryAtrium[A_0_30].ts1.constPinY = 15;
	m_Model.aryAtrium[A_0_30].ts2.constPinX = 175;
	m_Model.aryAtrium[A_0_30].ts2.constPinY = 22;

	m_Model.aryAtrium[A_1_0].ts1.constPinX = 183;
	m_Model.aryAtrium[A_1_0].ts1.constPinY = 27;
	m_Model.aryAtrium[A_1_0].ts2.constPinX = 195;
	m_Model.aryAtrium[A_1_0].ts2.constPinY = 35;

	m_Model.aryAtrium[A_L_BOTTOM].ts1.constPinX = 186;
	m_Model.aryAtrium[A_L_BOTTOM].ts1.constPinY = 44;
	m_Model.aryAtrium[A_L_BOTTOM].ts2.constPinX = 172;
	m_Model.aryAtrium[A_L_BOTTOM].ts2.constPinY = 57;

	m_Model.aryVentricle[V_7_30].ts1.constPinX = 43;
	m_Model.aryVentricle[V_7_30].ts1.constPinY = 171;
	m_Model.aryVentricle[V_7_30].ts2.constPinX = 52;
	m_Model.aryVentricle[V_7_30].ts2.constPinY = 182;

	m_Model.aryVentricle[V_7_0].ts1.constPinX = 60;
	m_Model.aryVentricle[V_7_0].ts1.constPinY = 189;
	m_Model.aryVentricle[V_7_0].ts2.constPinX = 73;
	m_Model.aryVentricle[V_7_0].ts2.constPinY = 200;

	m_Model.aryVentricle[V_6_30].ts1.constPinX = 81;
	m_Model.aryVentricle[V_6_30].ts1.constPinY = 205;
	m_Model.aryVentricle[V_6_30].ts2.constPinX = 97;
	m_Model.aryVentricle[V_6_30].ts2.constPinY = 211;

	m_Model.aryVentricle[V_6_0].ts1.constPinX = 106;
	m_Model.aryVentricle[V_6_0].ts1.constPinY = 213;
	m_Model.aryVentricle[V_6_0].ts2.constPinX = 124;
	m_Model.aryVentricle[V_6_0].ts2.constPinY = 216;

	m_Model.aryVentricle[V_5_30].ts1.constPinX = 133;
	m_Model.aryVentricle[V_5_30].ts1.constPinY = 216;
	m_Model.aryVentricle[V_5_30].ts2.constPinX = 151;
	m_Model.aryVentricle[V_5_30].ts2.constPinY = 214;

	m_Model.aryVentricle[V_5_0].ts1.constPinX = 159;
	m_Model.aryVentricle[V_5_0].ts1.constPinY = 211;
	m_Model.aryVentricle[V_5_0].ts2.constPinX = 175;
	m_Model.aryVentricle[V_5_0].ts2.constPinY = 204;

	m_Model.aryVentricle[V_4_30].ts1.constPinX = 183;
	m_Model.aryVentricle[V_4_30].ts1.constPinY = 200;
	m_Model.aryVentricle[V_4_30].ts2.constPinX = 197;
	m_Model.aryVentricle[V_4_30].ts2.constPinY = 189;

	m_Model.aryVentricle[V_4_0].ts1.constPinX = 204;
	m_Model.aryVentricle[V_4_0].ts1.constPinY = 182;
	m_Model.aryVentricle[V_4_0].ts2.constPinX = 214;
	m_Model.aryVentricle[V_4_0].ts2.constPinY = 168;

	m_Model.aryVentricle[V_3_30].ts1.constPinX = 220;
	m_Model.aryVentricle[V_3_30].ts1.constPinY = 160;
	m_Model.aryVentricle[V_3_30].ts2.constPinX = 226;
	m_Model.aryVentricle[V_3_30].ts2.constPinY = 145;

	m_Model.aryVentricle[V_3_0].ts1.constPinX = 229;
	m_Model.aryVentricle[V_3_0].ts1.constPinY = 135;
	m_Model.aryVentricle[V_3_0].ts2.constPinX = 231;
	m_Model.aryVentricle[V_3_0].ts2.constPinY = 117;

	m_Model.aryVentricle[V_2_30].ts1.constPinX = 231;
	m_Model.aryVentricle[V_2_30].ts1.constPinY = 109;
	m_Model.aryVentricle[V_2_30].ts2.constPinX = 228;
	m_Model.aryVentricle[V_2_30].ts2.constPinY = 91;

	m_Model.aryVentricle[V_2_0].ts1.constPinX = 226;
	m_Model.aryVentricle[V_2_0].ts1.constPinY = 82;
	m_Model.aryVentricle[V_2_0].ts2.constPinX = 219;
	m_Model.aryVentricle[V_2_0].ts2.constPinY = 66;

	m_Model.aryVentricle[V_1_30].ts1.constPinX = 215;
	m_Model.aryVentricle[V_1_30].ts1.constPinY = 58;
	m_Model.aryVentricle[V_1_30].ts2.constPinX = 206;
	m_Model.aryVentricle[V_1_30].ts2.constPinY = 46;
}

void CardiacConfig::SetTsName(TERMINAL_STATUS* pts, TCHAR* node_name, TCHAR* sub_name)
{
	CString s = node_name;
	s += sub_name;
	wcsncpy(pts->ts_name, s.GetBuffer(0), NAME_LEN_MAX);
}

void CardiacConfig::InitUnitName()
{
	//设定每个单元的名字
	wcsncpy(m_Model.suSinusNode.name, _T("窦房节"), NAME_LEN_MAX);
	SetTsName(&m_Model.suSinusNode.ts, m_Model.suSinusNode.name, _T(".ts"));
	wcsncpy(m_Model.suEctopicPacemakerOnRA.name, _T("右房异位源"), NAME_LEN_MAX);
	SetTsName(&m_Model.suEctopicPacemakerOnRA.ts, m_Model.suEctopicPacemakerOnRA.name, _T(".ts"));
	wcsncpy(m_Model.suEctopicPacemakerOnLA.name, _T("左房异位源"), NAME_LEN_MAX);
	SetTsName(&m_Model.suEctopicPacemakerOnLA.ts, m_Model.suEctopicPacemakerOnLA.name, _T(".ts"));
	wcsncpy(m_Model.suEctopicPacemakerOnRV.name, _T("右室异位源"), NAME_LEN_MAX);
	SetTsName(&m_Model.suEctopicPacemakerOnRV.ts, m_Model.suEctopicPacemakerOnRV.name, _T(".ts"));
	wcsncpy(m_Model.suEctopicPacemakerOnLV.name, _T("左室异位源"), NAME_LEN_MAX);
	SetTsName(&m_Model.suEctopicPacemakerOnLV.ts, m_Model.suEctopicPacemakerOnLV.name, _T(".ts"));
	wcsncpy(m_Model.suJunction.name, _T("交界区自律"), NAME_LEN_MAX);
	SetTsName(&m_Model.suJunction.ts, m_Model.suJunction.name, _T(".ts"));

	wcsncpy(m_Model.cuS_A.name, _T("房间束"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuS_A.ts1, m_Model.cuS_A.name, _T(".ts1"));
	SetTsName(&m_Model.cuS_A.ts2, m_Model.cuS_A.name, _T(".ts2"));
	wcsncpy(m_Model.cuJames.name, _T("James束"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuJames.ts1, m_Model.cuJames.name, _T(".ts1"));
	SetTsName(&m_Model.cuJames.ts2, m_Model.cuJames.name, _T(".ts2"));
	wcsncpy(m_Model.cuAVFast.name, _T("房室结快径"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuAVFast.ts1, m_Model.cuAVFast.name, _T(".ts1"));
	SetTsName(&m_Model.cuAVFast.ts2, m_Model.cuAVFast.name, _T(".ts2"));
	wcsncpy(m_Model.cuAVSlow.name, _T("房室结慢径"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuAVSlow.ts1, m_Model.cuAVSlow.name, _T(".ts1"));
	SetTsName(&m_Model.cuAVSlow.ts2, m_Model.cuAVSlow.name, _T(".ts2"));
	wcsncpy(m_Model.cuKentLeft.name, _T("左Kent束"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuKentLeft.ts1, m_Model.cuKentLeft.name, _T(".ts1"));
	SetTsName(&m_Model.cuKentLeft.ts2, m_Model.cuKentLeft.name, _T(".ts2"));
	wcsncpy(m_Model.cuKentRight.name, _T("右Kent束"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuKentRight.ts1, m_Model.cuKentRight.name, _T(".ts1"));
	SetTsName(&m_Model.cuKentRight.ts2, m_Model.cuKentRight.name, _T(".ts2"));
	wcsncpy(m_Model.cuHisBundle.name, _T("希氏束"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuHisBundle.ts1, m_Model.cuHisBundle.name, _T(".ts1"));
	SetTsName(&m_Model.cuHisBundle.ts2, m_Model.cuHisBundle.name, _T(".ts2"));
	wcsncpy(m_Model.cuIAS.name, _T("房间隔"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuIAS.ts1, m_Model.cuIAS.name, _T(".ts1"));
	SetTsName(&m_Model.cuIAS.ts2, m_Model.cuIAS.name, _T(".ts2"));
	wcsncpy(m_Model.cuIVS.name, _T("室间隔"), NAME_LEN_MAX);
	SetTsName(&m_Model.cuIVS.ts1, m_Model.cuIVS.name, _T(".ts1"));
	SetTsName(&m_Model.cuIVS.ts2, m_Model.cuIVS.name, _T(".ts2"));

	int i, count;
	count = sizeof(m_Model.aryBachmann) / sizeof(m_Model.aryBachmann[0]);
	for (i=0; i<count; i++) {
		wcsncpy(m_Model.aryBachmann[i].name, _aryBachmannLabel[i], NAME_LEN_MAX);
		SetTsName(&m_Model.aryBachmann[i].ts1, m_Model.aryBachmann[i].name, _T(".ts1"));
		SetTsName(&m_Model.aryBachmann[i].ts2, m_Model.aryBachmann[i].name, _T(".ts2"));
	}
	count = sizeof(m_Model.aryRBB) / sizeof(m_Model.aryRBB[0]);
	for (i=0; i<count; i++) {
		wcsncpy(m_Model.aryRBB[i].name, _aryRBBLabel[i], NAME_LEN_MAX);
		SetTsName(&m_Model.aryRBB[i].ts1, m_Model.aryRBB[i].name, _T(".ts1"));
		SetTsName(&m_Model.aryRBB[i].ts2, m_Model.aryRBB[i].name, _T(".ts2"));
	}
	count = sizeof(m_Model.aryLBB) / sizeof(m_Model.aryLBB[0]);
	for (i=0; i<count; i++) {
		wcsncpy(m_Model.aryLBB[i].name, _aryLBBLabel[i], NAME_LEN_MAX);
		SetTsName(&m_Model.aryLBB[i].ts1, m_Model.aryLBB[i].name, _T(".ts1"));
		SetTsName(&m_Model.aryLBB[i].ts2, m_Model.aryLBB[i].name, _T(".ts2"));
	}
	count = sizeof(m_Model.aryAtrium) / sizeof(m_Model.aryAtrium[0]);
	for (i=0; i<count; i++) {
		wcsncpy(m_Model.aryAtrium[i].name, _aryAtriumLabel[i], NAME_LEN_MAX);
		SetTsName(&m_Model.aryAtrium[i].ts1, m_Model.aryAtrium[i].name, _T(".ts1"));
		SetTsName(&m_Model.aryAtrium[i].ts2, m_Model.aryAtrium[i].name, _T(".ts2"));
	}
	count = sizeof(m_Model.aryVentricle) / sizeof(m_Model.aryVentricle[0]);
	for (i=0; i<count; i++) {
		wcsncpy(m_Model.aryVentricle[i].name, _aryVentricleLabel[i], NAME_LEN_MAX);
		SetTsName(&m_Model.aryVentricle[i].ts1, m_Model.aryVentricle[i].name, _T(".ts1"));
		SetTsName(&m_Model.aryVentricle[i].ts2, m_Model.aryVentricle[i].name, _T(".ts2"));
	}
}

void CardiacConfig::InitConnection()
{
	//设定单元之间的连接关系
	int i, count;
	SPONTANEOUS_UNIT** pSuGroup = GetSuGroup(&count);
	for (i=0; i<count; i++) {
		ZeroMemory(pSuGroup[i]->ts.aryInputSource,
			sizeof(int*) * UNIT_INPUT_MAX);
	}
	m_Model.suSinusNode.constSuppressedTimeDelay = 0;

	CONDUCTION_UNIT** pCuGroup = GetCuGroup(&count);
	for (i=0; i<count; i++) {
		ZeroMemory(pCuGroup[i]->ts1.aryInputSource,
			sizeof(int*) * UNIT_INPUT_MAX);
		ZeroMemory(pCuGroup[i]->ts2.aryInputSource,
			sizeof(int*) * UNIT_INPUT_MAX);
	}

	//连接心房右部
	JoinSingleChain(&m_Model.aryAtrium[A_R_BOTTOM], 4);

	//Bachmann束和心房的贴合部分
	JoinDuplexChain(&m_Model.aryAtrium[A_9_30], &m_Model.aryBachmann[BA_9_30], 4);

	//连接心房左部
	JoinSingleChain(&m_Model.aryAtrium[A_11_30], 5);

	//RBB和心室的贴合部分
	JoinDuplexChain(&m_Model.aryVentricle[V_7_30], &m_Model.aryRBB[RBB_7_30], 5);

	//LBB和心室的贴合部分
	JoinDuplexChain(&m_Model.aryVentricle[V_3_30], &m_Model.aryLBB[LBB_3_30], 5);

	//心室心尖部位的连接
	JoinSingleChain(&m_Model.aryVentricle[V_5_30], 3);

	//9_30位置的连接点（窦房节）
	AddConnection(&m_Model.suSinusNode.ts, &m_Model.aryBachmann[BA_9_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.suSinusNode.ts, &m_Model.aryAtrium[A_9_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.suSinusNode.ts, &m_Model.aryAtrium[A_9_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.suSinusNode.ts, &m_Model.cuS_A.ts1.nTerminalOutput);

	AddConnection(&m_Model.aryAtrium[A_9_0].ts2, &m_Model.suSinusNode.ts.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_9_0].ts2, &m_Model.aryBachmann[BA_9_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_9_0].ts2, &m_Model.aryAtrium[A_9_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_9_0].ts2, &m_Model.cuS_A.ts1.nTerminalOutput);

	AddConnection(&m_Model.aryAtrium[A_9_30].ts1, &m_Model.suSinusNode.ts.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_9_30].ts1, &m_Model.aryAtrium[A_9_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_9_30].ts1, &m_Model.cuS_A.ts1.nTerminalOutput);

	AddConnection(&m_Model.aryBachmann[BA_9_30].ts1, &m_Model.suSinusNode.ts.nTerminalOutput);
	AddConnection(&m_Model.aryBachmann[BA_9_30].ts1, &m_Model.aryAtrium[A_9_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryBachmann[BA_9_30].ts1, &m_Model.cuS_A.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuS_A.ts1, &m_Model.suSinusNode.ts.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts1, &m_Model.aryAtrium[A_9_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts1, &m_Model.aryAtrium[A_9_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts1, &m_Model.aryBachmann[BA_9_30].ts1.nTerminalOutput);

	//10_30位置的连接点（房间隔）
	AddConnection(&m_Model.cuIAS.ts1, &m_Model.aryAtrium[A_10_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts1, &m_Model.aryAtrium[A_10_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts1, &m_Model.aryBachmann[BA_10_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts1, &m_Model.aryBachmann[BA_10_30].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryAtrium[A_10_0].ts2, &m_Model.cuIAS.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_10_30].ts1, &m_Model.cuIAS.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryBachmann[BA_10_0].ts2, &m_Model.cuIAS.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryBachmann[BA_10_30].ts1, &m_Model.cuIAS.ts1.nTerminalOutput);

	//11_30位置的连接点
	AddConnection(&m_Model.aryAtrium[A_11_0].ts2, &m_Model.aryAtrium[A_11_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryBachmann[BA_11_0].ts2, &m_Model.aryAtrium[A_11_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_11_30].ts1, &m_Model.aryAtrium[A_11_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_11_30].ts1, &m_Model.aryBachmann[BA_11_0].ts2.nTerminalOutput);

	//1_0位置的连接点（左心房异位起搏点）
	AddConnection(&m_Model.suEctopicPacemakerOnLA.ts, &m_Model.aryAtrium[A_0_30].ts2.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnLA.ts, &m_Model.aryAtrium[A_1_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_0_30].ts2, &m_Model.suEctopicPacemakerOnLA.ts.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_1_0].ts1, &m_Model.suEctopicPacemakerOnLA.ts.nTerminalOutput);

	//1_30位置心房侧连接点（左心底和左Kent束）
	AddConnection(&m_Model.aryAtrium[A_L_BOTTOM].ts1, &m_Model.cuKentLeft.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_1_0].ts2, &m_Model.cuKentLeft.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuKentLeft.ts1, &m_Model.aryAtrium[A_L_BOTTOM].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuKentLeft.ts1, &m_Model.aryAtrium[A_1_0].ts2.nTerminalOutput);

	//1_30位置心室侧连接点（左Kent束）
	AddConnection(&m_Model.aryVentricle[V_1_30].ts2, &m_Model.cuKentLeft.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_1_30].ts2, &m_Model.cuKentLeft.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuKentLeft.ts2, &m_Model.aryVentricle[V_1_30].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuKentLeft.ts2, &m_Model.aryLBB[LBB_1_30].ts2.nTerminalOutput);

	//2_30位置的连接点（左心室异位起搏点）
	AddConnection(&m_Model.suEctopicPacemakerOnLV.ts, &m_Model.aryVentricle[V_2_30].ts2.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnLV.ts, &m_Model.aryVentricle[V_2_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnLV.ts, &m_Model.aryLBB[LBB_2_30].ts2.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnLV.ts, &m_Model.aryLBB[LBB_2_0].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_2_30].ts2, &m_Model.suEctopicPacemakerOnLV.ts.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_2_0].ts1, &m_Model.suEctopicPacemakerOnLV.ts.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_2_30].ts2, &m_Model.suEctopicPacemakerOnLV.ts.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_2_0].ts1, &m_Model.suEctopicPacemakerOnLV.ts.nTerminalOutput);

	//4_0位置的连接点
	AddConnection(&m_Model.aryLBB[LBB_3_30].ts1, &m_Model.aryLBB[LBB_4_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_3_30].ts1, &m_Model.aryVentricle[V_4_0].ts2.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_3_30].ts1, &m_Model.aryLBB[LBB_4_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_3_30].ts1, &m_Model.aryVentricle[V_4_0].ts2.nTerminalOutput);

	AddConnection(&m_Model.aryLBB[LBB_4_0].ts2, &m_Model.aryLBB[LBB_3_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_4_0].ts2, &m_Model.aryVentricle[V_4_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_4_0].ts2, &m_Model.aryVentricle[V_3_30].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_4_0].ts2, &m_Model.aryLBB[LBB_4_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_4_0].ts2, &m_Model.aryLBB[LBB_3_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_4_0].ts2, &m_Model.aryVentricle[V_3_30].ts1.nTerminalOutput);

	//4_30位置的连接点（两条分别连接，左束支根部连左束支，室间隔连心室）
	AddConnection(&m_Model.aryLBB[LBB_ROOT].ts2, &m_Model.aryLBB[LBB_4_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_4_0].ts1, &m_Model.aryLBB[LBB_ROOT].ts2.nTerminalOutput);

	AddConnection(&m_Model.cuIVS.ts2, &m_Model.aryVentricle[V_4_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuIVS.ts2, &m_Model.aryVentricle[V_4_30].ts2.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_4_30].ts2, &m_Model.cuIVS.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_4_30].ts2, &m_Model.aryVentricle[V_4_0].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_4_0].ts1, &m_Model.cuIVS.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_4_0].ts1, &m_Model.aryVentricle[V_4_30].ts2.nTerminalOutput);

	//5_0位置的连接点（两条分别连接，右束支根部连右束支，心室连心室）
	AddConnection(&m_Model.aryRBB[RBB_ROOT].ts1, &m_Model.aryRBB[RBB_5_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_5_0].ts2, &m_Model.aryRBB[RBB_ROOT].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_4_30].ts1, &m_Model.aryVentricle[V_5_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_5_0].ts2, &m_Model.aryVentricle[V_4_30].ts1.nTerminalOutput);

	//5_30位置的连接点
	AddConnection(&m_Model.aryRBB[RBB_5_30].ts2, &m_Model.aryRBB[RBB_5_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_5_30].ts2, &m_Model.aryVentricle[V_5_0].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_5_30].ts2, &m_Model.aryRBB[RBB_5_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_5_30].ts2, &m_Model.aryVentricle[V_5_0].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_5_0].ts1, &m_Model.aryRBB[V_5_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_5_0].ts1, &m_Model.aryRBB[RBB_5_30].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_5_0].ts1, &m_Model.aryVentricle[V_5_30].ts2.nTerminalOutput);

	AddConnection(&m_Model.aryRBB[RBB_5_0].ts1, &m_Model.aryRBB[RBB_5_30].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_5_0].ts1, &m_Model.aryVentricle[V_5_0].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_5_0].ts1, &m_Model.aryVentricle[V_5_30].ts2.nTerminalOutput);

	//7_0位置的连接点（右心室异位起搏点）
	AddConnection(&m_Model.suEctopicPacemakerOnRV.ts, &m_Model.aryVentricle[V_7_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnRV.ts, &m_Model.aryVentricle[V_6_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnRV.ts, &m_Model.aryRBB[RBB_7_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnRV.ts, &m_Model.aryRBB[RBB_6_30].ts1.nTerminalOutput);

	AddConnection(&m_Model.aryVentricle[V_7_0].ts2, &m_Model.suEctopicPacemakerOnRV.ts.nTerminalOutput);
	AddConnection(&m_Model.aryVentricle[V_6_30].ts1, &m_Model.suEctopicPacemakerOnRV.ts.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_7_0].ts2, &m_Model.suEctopicPacemakerOnRV.ts.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_6_30].ts1, &m_Model.suEctopicPacemakerOnRV.ts.nTerminalOutput);

	//8_0位置心室侧连接点（右Kent束）
	AddConnection(&m_Model.aryVentricle[V_7_30].ts1, &m_Model.cuKentRight.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_7_30].ts1, &m_Model.cuKentRight.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuKentRight.ts2, &m_Model.aryVentricle[V_7_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuKentRight.ts2, &m_Model.aryRBB[RBB_7_30].ts1.nTerminalOutput);

	//8_0位置心房侧连接点（右心底和右Kent束）
	AddConnection(&m_Model.aryAtrium[A_R_BOTTOM].ts2, &m_Model.cuKentRight.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_8_0].ts1, &m_Model.cuKentRight.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuKentRight.ts1, &m_Model.aryAtrium[A_R_BOTTOM].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuKentRight.ts1, &m_Model.aryAtrium[A_8_0].ts1.nTerminalOutput);

	//8_30位置的连接点（右心房异位起搏点）
	AddConnection(&m_Model.suEctopicPacemakerOnRA.ts, &m_Model.aryAtrium[A_8_0].ts2.nTerminalOutput);
	AddConnection(&m_Model.suEctopicPacemakerOnRA.ts, &m_Model.aryAtrium[A_8_30].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_8_0].ts2, &m_Model.suEctopicPacemakerOnRA.ts.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_8_30].ts1, &m_Model.suEctopicPacemakerOnRA.ts.nTerminalOutput);

	//房室结心房端的连接点
	AddConnection(&m_Model.cuS_A.ts2, &m_Model.cuIAS.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts2, &m_Model.aryAtrium[A_L_BOTTOM].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts2, &m_Model.aryAtrium[A_R_BOTTOM].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts2, &m_Model.cuJames.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts2, &m_Model.cuAVFast.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuS_A.ts2, &m_Model.cuAVSlow.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuIAS.ts2, &m_Model.cuS_A.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts2, &m_Model.aryAtrium[A_L_BOTTOM].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts2, &m_Model.aryAtrium[A_R_BOTTOM].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts2, &m_Model.cuJames.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts2, &m_Model.cuAVFast.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuIAS.ts2, &m_Model.cuAVSlow.ts1.nTerminalOutput);

	AddConnection(&m_Model.aryAtrium[A_L_BOTTOM].ts2, &m_Model.cuS_A.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_L_BOTTOM].ts2, &m_Model.cuIAS.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_L_BOTTOM].ts2, &m_Model.aryAtrium[A_R_BOTTOM].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_L_BOTTOM].ts2, &m_Model.cuJames.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_L_BOTTOM].ts2, &m_Model.cuAVFast.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_L_BOTTOM].ts2, &m_Model.cuAVSlow.ts1.nTerminalOutput);

	AddConnection(&m_Model.aryAtrium[A_R_BOTTOM].ts1, &m_Model.cuIAS.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_R_BOTTOM].ts1, &m_Model.aryAtrium[A_L_BOTTOM].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_R_BOTTOM].ts1, &m_Model.cuS_A.ts2.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_R_BOTTOM].ts1, &m_Model.cuJames.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_R_BOTTOM].ts1, &m_Model.cuAVFast.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryAtrium[A_R_BOTTOM].ts1, &m_Model.cuAVSlow.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuJames.ts1, &m_Model.cuIAS.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts1, &m_Model.aryAtrium[A_L_BOTTOM].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts1, &m_Model.aryAtrium[A_R_BOTTOM].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts1, &m_Model.cuS_A.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts1, &m_Model.cuAVFast.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts1, &m_Model.cuAVSlow.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuAVFast.ts1, &m_Model.cuIAS.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts1, &m_Model.aryAtrium[A_L_BOTTOM].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts1, &m_Model.aryAtrium[A_R_BOTTOM].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts1, &m_Model.cuS_A.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts1, &m_Model.cuJames.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts1, &m_Model.cuAVSlow.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuAVSlow.ts1, &m_Model.cuIAS.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts1, &m_Model.aryAtrium[A_L_BOTTOM].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts1, &m_Model.aryAtrium[A_R_BOTTOM].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts1, &m_Model.cuS_A.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts1, &m_Model.cuAVFast.ts1.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts1, &m_Model.cuJames.ts1.nTerminalOutput);

	//房室结心室端的连接点
	AddConnection(&m_Model.suJunction.ts, &m_Model.cuAVFast.ts2.nTerminalOutput);
	AddConnection(&m_Model.suJunction.ts, &m_Model.cuAVSlow.ts2.nTerminalOutput);
	AddConnection(&m_Model.suJunction.ts, &m_Model.cuJames.ts2.nTerminalOutput);
	AddConnection(&m_Model.suJunction.ts, &m_Model.cuHisBundle.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuAVFast.ts2, &m_Model.suJunction.ts.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts2, &m_Model.cuAVSlow.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts2, &m_Model.cuJames.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVFast.ts2, &m_Model.cuHisBundle.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuAVSlow.ts2, &m_Model.suJunction.ts.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts2, &m_Model.cuAVFast.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts2, &m_Model.cuJames.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuAVSlow.ts2, &m_Model.cuHisBundle.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuJames.ts2, &m_Model.suJunction.ts.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts2, &m_Model.cuAVFast.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts2, &m_Model.cuAVSlow.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuJames.ts2, &m_Model.cuHisBundle.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuHisBundle.ts1, &m_Model.suJunction.ts.nTerminalOutput);
	AddConnection(&m_Model.cuHisBundle.ts1, &m_Model.cuAVFast.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuHisBundle.ts1, &m_Model.cuAVSlow.ts2.nTerminalOutput);
	AddConnection(&m_Model.cuHisBundle.ts1, &m_Model.cuJames.ts2.nTerminalOutput);

	//希式束与左右分支的连接点
	AddConnection(&m_Model.cuHisBundle.ts2, &m_Model.aryLBB[LBB_ROOT].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuHisBundle.ts2, &m_Model.aryRBB[RBB_ROOT].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuHisBundle.ts2, &m_Model.cuIVS.ts1.nTerminalOutput);

	AddConnection(&m_Model.cuIVS.ts1, &m_Model.aryLBB[LBB_ROOT].ts1.nTerminalOutput);
	AddConnection(&m_Model.cuIVS.ts1, &m_Model.aryRBB[RBB_ROOT].ts2.nTerminalOutput);
	AddConnection(&m_Model.cuIVS.ts1, &m_Model.cuHisBundle.ts2.nTerminalOutput);

	AddConnection(&m_Model.aryLBB[LBB_ROOT].ts1, &m_Model.aryRBB[RBB_ROOT].ts2.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_ROOT].ts1, &m_Model.cuIVS.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryLBB[LBB_ROOT].ts1, &m_Model.cuHisBundle.ts2.nTerminalOutput);

	AddConnection(&m_Model.aryRBB[RBB_ROOT].ts2, &m_Model.aryLBB[LBB_ROOT].ts1.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_ROOT].ts2, &m_Model.cuIVS.ts1.nTerminalOutput);
	AddConnection(&m_Model.aryRBB[RBB_ROOT].ts2, &m_Model.cuHisBundle.ts2.nTerminalOutput);
}

void CardiacConfig::Reset()
{
	int i, count;
	SPONTANEOUS_UNIT** pSuGroup = GetSuGroup(&count);
	for (i=0; i<count; i++) {
		pSuGroup[i]->nInactiveTimeDelay = 800;
		pSuGroup[i]->constSuppressedTimeDelay = 800; //除窦房结之外，其他起搏点都压制0.8秒钟时间
		pSuGroup[i]->ts.nStatus = 0;
		pSuGroup[i]->ts.nStatusTimeCounter = 0;
		pSuGroup[i]->ts.nTerminalOutput = 0;
	}
	m_Model.suSinusNode.nInactiveTimeDelay = 0;
	m_Model.suSinusNode.constSuppressedTimeDelay = 0;

	CONDUCTION_UNIT** pCuGroup = GetCuGroup(&count);
	for (i=0; i<count; i++) {
		pCuGroup[i]->nConductionCounter = 0;
		pCuGroup[i]->nFirstActive = 0;
		pCuGroup[i]->ts1.nStatus = 0;
		pCuGroup[i]->ts1.nStatusTimeCounter = 0;
		pCuGroup[i]->ts1.nTerminalOutput = 0;
		pCuGroup[i]->ts2.nStatus = 0;
		pCuGroup[i]->ts2.nStatusTimeCounter = 0;
		pCuGroup[i]->ts2.nTerminalOutput = 0;
	}
}

void CardiacConfig::InitNormal()
{
	int i, count;

	//初始化一颗正常状态的心脏
	//关闭所有异位传导，以及所有异位起搏点
	m_Model.suEctopicPacemakerOnLA.bOutputBlock = TRUE;
	m_Model.suEctopicPacemakerOnLV.bOutputBlock = TRUE;
	m_Model.suEctopicPacemakerOnRA.bOutputBlock = TRUE;
	m_Model.suEctopicPacemakerOnRV.bOutputBlock = TRUE;
	m_Model.cuJames.bCompleteBlock = TRUE;
	m_Model.cuKentLeft.bCompleteBlock = TRUE;
	m_Model.cuKentRight.bCompleteBlock = TRUE;
	
	//初始化窦房结的频率为75次每分钟
	SPONTANEOUS_UNIT& sin = m_Model.suSinusNode;
	sin.bOutputBlock = FALSE;
	sin.fBaseFreqency = 75.0;
	sin.fGaussianVariance = 15.0;   //建议大于10.0
	sin.nInactiveTimeDelay = 0;
	sin.constSuppressedTimeDelay = 500; //如果受到外来刺激，则压制0.5秒钟时间
	sin.ts.nStatus = TERMINAL_STATUS_RESTING;
	sin.ts.constActiveRatio = 0.02f;
	sin.ts.constRefractoryRatio = 0.25f;

	//初始化房室结的频率为50次每分钟
	SPONTANEOUS_UNIT& avn = m_Model.suJunction;
	avn.bOutputBlock = FALSE;
	avn.fBaseFreqency = 50.0;
	avn.fGaussianVariance = 15.0;   //建议大于10.0
	avn.nInactiveTimeDelay = 400;
	avn.constSuppressedTimeDelay = 800; //如果受到外来刺激，则压制0.8秒钟时间
	avn.ts.nStatus = TERMINAL_STATUS_RESTING;
	avn.ts.constActiveRatio = 0.02f;
	avn.ts.constRefractoryRatio = 0.25f;

	//初始化Bachmann束
	count = sizeof(m_Model.aryBachmann) / sizeof(m_Model.aryBachmann[0]);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT& ba = m_Model.aryBachmann[i];
		ba.bCompleteBlock = FALSE;
		ba.constConductionTime = 15;
		ba.ts1.nStatus = TERMINAL_STATUS_RESTING;
		ba.ts1.constActivePeriod = 20;
		ba.ts1.constRefractoryPeriod = 270;
		ba.ts2.nStatus = TERMINAL_STATUS_RESTING;
		ba.ts2.constActivePeriod = 20;
		ba.ts2.constRefractoryPeriod = 270;
	}

	//初始化房间束
	CONDUCTION_UNIT& sa = m_Model.cuS_A;
	sa.bCompleteBlock = FALSE;
	sa.constConductionTime = 120;
	sa.ts1.nStatus = TERMINAL_STATUS_RESTING;
	sa.ts1.constActivePeriod = 20;
	sa.ts1.constRefractoryPeriod = 220;
	sa.ts2.nStatus = TERMINAL_STATUS_RESTING;
	sa.ts2.constActivePeriod = 20;
	sa.ts2.constRefractoryPeriod = 220;

	//初始化房室结快路径
	CONDUCTION_UNIT& avf = m_Model.cuAVFast;
	avf.bCompleteBlock = FALSE;
	avf.constConductionTime = 40;
	avf.ts1.nStatus = TERMINAL_STATUS_RESTING;
	avf.ts1.constActivePeriod = 20;
	avf.ts1.constRefractoryPeriod = 190;
	avf.ts2.nStatus = TERMINAL_STATUS_RESTING;
	avf.ts2.constActivePeriod = 20;
	avf.ts2.constRefractoryPeriod = 190;

	//初始化房室结慢路径
	CONDUCTION_UNIT& avs = m_Model.cuAVSlow;
	avs.bCompleteBlock = FALSE;
	avs.constConductionTime = 80;
	avs.ts1.nStatus = TERMINAL_STATUS_RESTING;
	avs.ts1.constActivePeriod = 20;
	avs.ts1.constRefractoryPeriod = 220;
	avs.ts2.nStatus = TERMINAL_STATUS_RESTING;
	avs.ts2.constActivePeriod = 20;
	avs.ts2.constRefractoryPeriod = 220;

	//初始化希氏束
	CONDUCTION_UNIT& his = m_Model.cuHisBundle;
	his.bCompleteBlock = FALSE;
	his.constConductionTime = 20;
	his.ts1.nStatus = TERMINAL_STATUS_RESTING;
	his.ts1.constActivePeriod = 20;
	his.ts1.constRefractoryPeriod = 220;
	his.ts2.nStatus = TERMINAL_STATUS_RESTING;
	his.ts2.constActivePeriod = 20;
	his.ts2.constRefractoryPeriod = 220;

	//初始化右束支
	count = sizeof(m_Model.aryRBB) / sizeof(m_Model.aryRBB[0]);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT& rbb = m_Model.aryRBB[i];
		rbb.bCompleteBlock = FALSE;
		rbb.constConductionTime = 20;
		rbb.ts1.nStatus = TERMINAL_STATUS_RESTING;
		rbb.ts1.constActivePeriod = 20;
		rbb.ts1.constRefractoryPeriod = 220;
		rbb.ts2.nStatus = TERMINAL_STATUS_RESTING;
		rbb.ts2.constActivePeriod = 20;
		rbb.ts2.constRefractoryPeriod = 220;
	}

	//初始化左束支
	count = sizeof(m_Model.aryLBB) / sizeof(m_Model.aryLBB[0]);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT& lbb = m_Model.aryLBB[i];
		lbb.bCompleteBlock = FALSE;
		lbb.constConductionTime = 27;
		lbb.ts1.nStatus = TERMINAL_STATUS_RESTING;
		lbb.ts1.constActivePeriod = 20;
		lbb.ts1.constRefractoryPeriod = 220;
		lbb.ts2.nStatus = TERMINAL_STATUS_RESTING;
		lbb.ts2.constActivePeriod = 20;
		lbb.ts2.constRefractoryPeriod = 220;
	}

	//初始房间隔
	CONDUCTION_UNIT& ias = m_Model.cuIAS;
	ias.bCompleteBlock = FALSE;
	ias.constConductionTime = 40;
	ias.ts1.nStatus = TERMINAL_STATUS_RESTING;
	ias.ts1.constActivePeriod = 20;
	ias.ts1.constRefractoryPeriod = 160;
	ias.ts2.nStatus = TERMINAL_STATUS_RESTING;
	ias.ts2.constActivePeriod = 20;
	ias.ts2.constRefractoryPeriod = 160;

	//初始化心房肌
	count = sizeof(m_Model.aryAtrium) / sizeof(m_Model.aryAtrium[0]);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT& a = m_Model.aryAtrium[i];
		a.bCompleteBlock = FALSE;
		a.constConductionTime = 40;
		a.ts1.nStatus = TERMINAL_STATUS_RESTING;
		a.ts1.constActivePeriod = 20;
		a.ts1.constRefractoryPeriod = 160;
		a.ts2.nStatus = TERMINAL_STATUS_RESTING;
		a.ts2.constActivePeriod = 20;
		a.ts2.constRefractoryPeriod = 160;
	}

	//初始化室间隔
	CONDUCTION_UNIT& ivs = m_Model.cuIVS;
	ivs.bCompleteBlock = FALSE;
	ivs.constConductionTime = 40;
	ivs.ts1.nStatus = TERMINAL_STATUS_RESTING;
	ivs.ts1.constActivePeriod = 20;
	ivs.ts1.constRefractoryPeriod = 220;
	ivs.ts2.nStatus = TERMINAL_STATUS_RESTING;
	ivs.ts2.constActivePeriod = 20;
	ivs.ts2.constRefractoryPeriod = 200;

	//初始化心室肌（因为心室有浦肯野纤维，所以传导速度很快）
	count = sizeof(m_Model.aryVentricle) / sizeof(m_Model.aryVentricle[0]);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT& v = m_Model.aryVentricle[i];
		v.bCompleteBlock = FALSE;
		v.constConductionTime = 40;
		v.ts1.nStatus = TERMINAL_STATUS_RESTING;
		v.ts1.constActivePeriod = 20;
		v.ts1.constRefractoryPeriod = 200;
		v.ts2.nStatus = TERMINAL_STATUS_RESTING;
		v.ts2.constActivePeriod = 20;
		v.ts2.constRefractoryPeriod = 200;
	}
}

// 初始化正态分布数组
void CardiacConfig::InitUniform()
{
	int i, count;
	SPONTANEOUS_UNIT** pSuGroup = GetSuGroup(&count);
	for (i=0; i<count; i++) {

		SPONTANEOUS_UNIT* psu = pSuGroup[i];
		if (!psu->bOutputBlock && psu->fBaseFreqency > 0) {
			double miu = 60000.0 / psu->fBaseFreqency;
			std::default_random_engine generator;
			std::normal_distribution<double> distribution(miu, psu->fGaussianVariance);

			psu->m_nUniformIndex = 0;
			int k;
			for (k=0; k<UNIFORM_NUM_COUNT; k++) {
				psu->m_aryUniformNum[k] = (int)distribution(generator);
			}
		}
	}
}

// 计算1ms之后的各个端点状态
void CardiacConfig::Click_1ms()
{
	int i, count;
	SPONTANEOUS_UNIT** pSuGroup = GetSuGroup(&count);
	for (i=0; i<count; i++) {
		SpontaneousUnitProc(pSuGroup[i]);
	}

	CONDUCTION_UNIT** pCuGroup = GetCuGroup(&count);
	for (i=0; i<count; i++) {
		CondictionUnitProc(pCuGroup[i]);
	}
}

void CardiacConfig::SpontaneousUnitProc(SPONTANEOUS_UNIT* psu)
{
	if (psu->bOutputBlock) {
		return;
	}

	//检查是否会被外来刺激所抑制
	//如果遇到输入，则会被抑制一个较长的时间
	if (psu->ts.nStatus == TERMINAL_STATUS_RESTING) {
		int t;
		for (t=0; t<UNIT_INPUT_MAX; t++) {
			int* pInput = psu->ts.aryInputSource[t];
			if (! pInput) {
				break;
			}
			if (* pInput == TERMINAL_OUTPUT_ACTIVE) {
				//被外来的激动抑制了
				psu->ts.nStatusTimeCounter = 0;
				psu->ts.nTerminalOutput = TERMINAL_OUTPUT_REFRACTORY;
				if (psu->constSuppressedTimeDelay > psu->nInactiveTimeDelay) {
					psu->nInactiveTimeDelay = psu->constSuppressedTimeDelay;
				}
				break;
			}
		}
	}

	//第一次执行可以直接激活，这里要设置好初始化参数
	if (0 == psu->nInactiveTimeDelay) {
		int gap = GetBeatGap(psu);
		psu->ts.constActivePeriod = (int)(gap * psu->ts.constActiveRatio);
		psu->ts.constRefractoryPeriod = (int)(gap * psu->ts.constRefractoryRatio);
	}

	//逻辑是：先延迟，延迟结束之后再激活
	switch (psu->ts.nStatus) {
	//静息
	case TERMINAL_STATUS_RESTING:
		if (psu->ts.nStatusTimeCounter >= psu->nInactiveTimeDelay) {
			psu->ts.nStatus = TERMINAL_STATUS_POLARIZED;
			psu->ts.nTerminalOutput = TERMINAL_OUTPUT_ACTIVE;
		}
		break;

	//极化状态
	case TERMINAL_STATUS_POLARIZED:
		if (psu->ts.nStatusTimeCounter >= 
			psu->nInactiveTimeDelay + psu->ts.constActivePeriod) {
			//解除输出端的激活状态
			psu->ts.nTerminalOutput = TERMINAL_OUTPUT_REFRACTORY;
		}
		if (psu->ts.nStatusTimeCounter >=
			psu->nInactiveTimeDelay + psu->ts.constRefractoryPeriod) {
			//超过绝对不应期，解除极化状态
			psu->ts.nStatus = TERMINAL_STATUS_RESTING;
			psu->ts.nTerminalOutput = TERMINAL_OUTPUT_INACTIVE;

			//周期结束，设定下一次自主节律的延迟时间
			int gap = GetBeatGap(psu);
			psu->ts.constActivePeriod = (int)(gap * psu->ts.constActiveRatio);
			psu->ts.constRefractoryPeriod = (int)(gap * psu->ts.constRefractoryRatio);
			psu->nInactiveTimeDelay = gap - psu->ts.constRefractoryPeriod;
			psu->ts.nStatusTimeCounter = 0;
		}
		break;
	}

	psu->ts.nStatusTimeCounter ++;
}

// 计算member在type中的位置
// #define offsetof(type, member)  (size_t)(&((type*)0)->member)
// 根据member的地址获取type的起始地址
// #define container_of(ptr, type, member) ({          \
//         const typeof(((type *)0)->member)*__mptr = (ptr);    \
//     (type *)((char *)__mptr - offsetof(type, member)); })
void CardiacConfig::GetTsPos(int* pTerminalOutput, CString& sName)
{
	static const int offset = (int)(&((TERMINAL_STATUS *)0)->nTerminalOutput);
	TERMINAL_STATUS* pTS = (TERMINAL_STATUS *)(((char *)pTerminalOutput) - offset);
	sName = pTS->ts_name;
}

void CardiacConfig::CondictionUnitProc(CONDUCTION_UNIT* pcu)
{
	if (pcu->bCompleteBlock) {
		return;
	}

	BOOL bIsActive;

	// 推进传导
	if (0 != pcu->nFirstActive) {
		pcu->nConductionCounter ++;

		//如果传导落在对侧的不应期，则湮灭
		if (pcu->nConductionCounter >= pcu->constConductionTime) {
			if ((1 == pcu->nFirstActive && TERMINAL_STATUS_POLARIZED == pcu->ts2.nStatus)
			  ||(2 == pcu->nFirstActive && TERMINAL_STATUS_POLARIZED == pcu->ts1.nStatus)){
					pcu->nFirstActive = 0;
					pcu->nConductionCounter = 0;
			}
		}
	}

	// 处理第一个节点
	switch (pcu->ts1.nStatus) {
	//静息
	case TERMINAL_STATUS_RESTING:

		//检查是否被外来刺激所激活
		//如果在静息时遇到输入，则会被激活
		bIsActive = FALSE;
		int t;
		for (t=0; t<UNIT_INPUT_MAX; t++) {
			int* pInput = pcu->ts1.aryInputSource[t];
			if (! pInput) {
				break;
			}
			if (* pInput == TERMINAL_OUTPUT_ACTIVE) {
				if (0 == pcu->nFirstActive
					&& ! pcu->b1to2Block) {
					pcu->nFirstActive = 1;
				}
				else if (2 == pcu->nFirstActive) {
					//如果对面正在传递，则会被湮灭
					pcu->nFirstActive = 0;
					pcu->nConductionCounter = 0;
				}

				bIsActive = TRUE;

				CString sInputName;
				GetTsPos(pInput, sInputName);
				CString info;
				info.Format(_T("%s was actived by %s\n"),
					pcu->ts1.ts_name, sInputName);
				OutputDebugString(info);
				break;
			}
		}

		//检查传导是否会激活本节点
		if (2 == pcu->nFirstActive //对侧过来的传导
			&& pcu->nConductionCounter >= pcu->constConductionTime) {

				pcu->nFirstActive = 0;
				pcu->nConductionCounter = 0;
				bIsActive = TRUE;

				CString info;
				info.Format(_T("%s was actived by 对侧\n"), pcu->ts1.ts_name);
				OutputDebugString(info);
		}

		if (bIsActive) {
			pcu->ts1.nStatus = TERMINAL_STATUS_POLARIZED;
			pcu->ts1.nTerminalOutput = TERMINAL_OUTPUT_ACTIVE;
			pcu->ts1.nStatusTimeCounter = 0;
			break;
		}
		break;

	//极化状态
	case TERMINAL_STATUS_POLARIZED:
		pcu->ts1.nStatusTimeCounter ++;
		if (pcu->ts1.nStatusTimeCounter == pcu->ts1.constActivePeriod) {
			//解除端点的激活状态
			pcu->ts1.nTerminalOutput = TERMINAL_OUTPUT_REFRACTORY;
		}
		if (pcu->ts1.nStatusTimeCounter >= pcu->ts1.constRefractoryPeriod) {
			//超过绝对不应期，解除极化状态
			pcu->ts1.nStatus = TERMINAL_STATUS_RESTING;
			pcu->ts1.nTerminalOutput = TERMINAL_OUTPUT_INACTIVE;
			pcu->ts1.nStatusTimeCounter = 0;
		}
		break;
	}

	// 处理第二个节点
	switch (pcu->ts2.nStatus) {
	//静息
	case TERMINAL_STATUS_RESTING:

		//检查是否被外来刺激所激活
		//如果在静息时遇到输入，则会被激活
		bIsActive = FALSE;
		int t;
		for (t=0; t<UNIT_INPUT_MAX; t++) {
			int* pInput = pcu->ts2.aryInputSource[t];
			if (! pInput) {
				break;
			}
			if (* pInput == TERMINAL_OUTPUT_ACTIVE) {
				//if (0 == wcscmp(pcu->name, _T("房室结慢径"))) {
				//	int n = 1;
				//}

				if (0 == pcu->nFirstActive
					&& ! pcu->b2to1Block) {
					pcu->nFirstActive = 2;
				}
				else if (1 == pcu->nFirstActive) {
					//如果对面正在传递，则会被湮灭
					pcu->nFirstActive = 0;
					pcu->nConductionCounter = 0;
				}
				bIsActive = TRUE;

				CString sInputName;
				GetTsPos(pInput, sInputName);
				CString info;
				info.Format(_T("%s was actived by %s\n"),
					pcu->ts2.ts_name, sInputName);
				OutputDebugString(info);
				break;
			}
		}

		//检查传导是否会激活本节点
		if (1 == pcu->nFirstActive //对侧过来的传导
			&& pcu->nConductionCounter >= pcu->constConductionTime) {

				pcu->nFirstActive = 0;
				pcu->nConductionCounter = 0;
				bIsActive = TRUE;

				CString info;
				info.Format(_T("%s was actived by 对侧\n"), pcu->ts2.ts_name);
				OutputDebugString(info);
		}

		if (bIsActive) {
			pcu->ts2.nStatus = TERMINAL_STATUS_POLARIZED;
			pcu->ts2.nTerminalOutput = TERMINAL_OUTPUT_ACTIVE;
			pcu->ts2.nStatusTimeCounter = 0;
			break;
		}
		break;

	//极化状态
	case TERMINAL_STATUS_POLARIZED:
		pcu->ts2.nStatusTimeCounter ++;
		if (pcu->ts2.nStatusTimeCounter == pcu->ts2.constActivePeriod) {
			//解除端点的激活状态
			pcu->ts2.nTerminalOutput = TERMINAL_OUTPUT_REFRACTORY;
		}
		if (pcu->ts2.nStatusTimeCounter >= pcu->ts2.constRefractoryPeriod) {
			//超过绝对不应期，解除极化状态
			pcu->ts2.nStatus = TERMINAL_STATUS_RESTING;
			pcu->ts2.nTerminalOutput = TERMINAL_OUTPUT_INACTIVE;
			pcu->ts2.nStatusTimeCounter;
		}
		break;
	}
}

BOOL CardiacConfig::Save(LPCTSTR lpszPathName)
{
	CFile file;
	if (file.Open(lpszPathName, 
		CFile::modeCreate | CFile::modeWrite | CFile::typeBinary)) {
		int size = sizeof(m_Model);
		file.Write((BYTE*)&m_Model, size);
		file.Close();
		return TRUE;
	}
	return FALSE;
}

BOOL CardiacConfig::Load(LPCTSTR lpszPathName)
{
	OutputDebugString(_T("--------------- LOAD ---------------\n"));
	CFile file;
	if (file.Open(lpszPathName,
		CFile::modeRead | CFile::typeBinary)) {
		int size = sizeof(m_Model);
		int n = file.Read((BYTE*)&m_Model, size);
		file.Close();

		InitConnection();
		Reset();
		return TRUE;
	}
	return FALSE;
}
