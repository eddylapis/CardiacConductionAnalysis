
// CCADoc.cpp : CCADoc 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "CardiacConductionAnalysis.h"
#include "CardiacConfig.h"
#endif

#include "MainFrm.h"
#include "CCADoc.h"
#include "CCAView.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CCADoc

IMPLEMENT_DYNCREATE(CCADoc, CDocument)

BEGIN_MESSAGE_MAP(CCADoc, CDocument)
END_MESSAGE_MAP()


// CCADoc 构造/析构

CCADoc::CCADoc()
{
	// TODO: 在此添加一次性构造代码
	m_pData = new CardiacConfig();
	m_pData->InitNormal();
	theApp.m_pDoc = this;
}

CCADoc::~CCADoc()
{
	theApp.m_pDoc = NULL;
	delete m_pData;
}

BOOL CCADoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	m_pData->InitNormal();

	static BOOL isFirst = TRUE;
	if (isFirst) {
		isFirst = FALSE;
	}
	else {
		theApp.m_pView->RenewBarChart();
	}
	return TRUE;
}


// CCADoc 序列化

void CCADoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}

#ifdef SHARED_HANDLERS

// 缩略图的支持
void CCADoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// 修改此代码以绘制文档数据
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// 搜索处理程序的支持
void CCADoc::InitializeSearchContent()
{
	CString strSearchContent;
	// 从文档数据设置搜索内容。
	// 内容部分应由“;”分隔

	// 例如:  strSearchContent = _T("point;rectangle;circle;ole object;")；
	SetSearchContent(strSearchContent);
}

void CCADoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CCADoc 诊断

#ifdef _DEBUG
void CCADoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCADoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CCADoc 命令
BOOL CCADoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	//if (!CDocument::OnOpenDocument(lpszPathName))
	//	return FALSE;

	m_pData->Load(lpszPathName);
	theApp.m_pView->RenewBarChart();

	SetModifiedFlag(FALSE);//清除修改标志
	return TRUE;
}

BOOL CCADoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	m_pData->Save(lpszPathName);

	SetModifiedFlag(FALSE);//清除修改标志
	return TRUE;

	//坑，默认操作会把文件清空！
	//return CDocument::OnSaveDocument(lpszPathName);
}
