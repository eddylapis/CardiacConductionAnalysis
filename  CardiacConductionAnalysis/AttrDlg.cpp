// AttrDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "CardiacConductionAnalysis.h"
#include "AttrDlg.h"
#include "afxdialogex.h"
#include "SmartDC.h"
#include "CCAView.h"

//column上面显示的文字
#define NUM_COLUMNS 	2	//2列

static _TCHAR *_gszColumnLabel[NUM_COLUMNS] =
{
	_T("属性名"),
	_T("属性值")
};
//column上文字的显示方式（靠左）
static int _gnColumnFmt[NUM_COLUMNS] = 
{
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
static int _gnColumnWidth[NUM_COLUMNS] = 
{
	260,
	60
};

// CAttrDlg 对话框
IMPLEMENT_DYNAMIC(CAttrDlg, CDialogEx)

CAttrDlg::CAttrDlg(CWnd* pParent, CardiacConfig* pConfig,
	CSmartDC* pSketchDC, int nSketchW, int nSketchH)
	: CDialogEx(CAttrDlg::IDD, pParent)
	, m_pSketchDC(pSketchDC)
	, m_nSketchW(nSketchW)
	, m_nSketchH(nSketchH)
	, m_bSinusNode(FALSE)
	, m_bPacemakerOnRA(FALSE)
	, m_bPacemakerOnLA(FALSE)
	, m_bPacemakerOnRV(FALSE)
	, m_bPacemakerOnLV(FALSE)
	, m_bJunction(FALSE)
	, m_bS_A(FALSE)
	, m_bBachmann(FALSE)
	, m_bJames(FALSE)
	, m_bAVFast(FALSE)
	, m_bAVSlow(FALSE)
	, m_bKentLeft(FALSE)
	, m_bKentRight(FALSE)
	, m_bHisBundle(FALSE)
	, m_bRBB(FALSE)
	, m_bLBB(FALSE)
	, m_bIAS(FALSE)
	, m_bIVS(FALSE)
	, m_bAtrium(FALSE)
	, m_bVentricule(FALSE)
	, m_bCheckAll(FALSE)
	, m_nAttrListIndexMax(0)
	, m_bEditEnableState(FALSE)
{
	pConfig->CopyTo(&m_setup);

	//CString info;
	//info.Format(_T("AttrDlg::CAttrDlg: %d\n"), m_setup.aryRBB[0].constConductionTime);
	//OutputDebugString(info);

	//测试小括号操作符
	//int I = (OutputDebugString(_T("Hello World !\n")), 10);
	//wchar_t buffer[20];
	//wsprintf(buffer, _T("I = %d\n"), I);
	//OutputDebugString(buffer);
}

CAttrDlg::~CAttrDlg()
{
}

void CAttrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ATTR_LIST, m_lstAttr);
	DDX_Control(pDX, IDC_CHECK_ALL, m_btnCheckAll);
	DDX_Control(pDX, IDC_SINUS_NODE, m_btnSinusNode);
	DDX_Control(pDX, IDC_EP_ON_RA, m_btnPacemakerOnRA);
	DDX_Control(pDX, IDC_EP_ON_LA, m_btnPacemakerOnLA);
	DDX_Control(pDX, IDC_EP_ON_RV, m_btnPacemakerOnRV);
	DDX_Control(pDX, IDC_EP_ON_LV, m_btnPacemakerOnLV);
	DDX_Control(pDX, IDC_JUNCTION, m_btnJunction);
	DDX_Control(pDX, IDC_S_A, m_btnS_A);
	DDX_Control(pDX, IDC_BACHMANN, m_btnBachmann);
	DDX_Control(pDX, IDC_JAMES, m_btnJames);
	DDX_Control(pDX, IDC_AV_FAST, m_vtnAVFast);
	DDX_Control(pDX, IDC_AV_SLOW, m_btnAVSlow);
	DDX_Control(pDX, IDC_KENT_LEFT, m_btnKentLeft);
	DDX_Control(pDX, IDC_KENT_RIGHT, m_btnKentRight);
	DDX_Control(pDX, IDC_HIS_BUNDLE, m_btnHisBundle);
	DDX_Control(pDX, IDC_RIGHT_BUNDLE_BRANCH, m_btnRBB);
	DDX_Control(pDX, IDC_LEFT_BUNDLE_BRANCH, m_btnLBB);
	DDX_Control(pDX, IDC_IAS, m_btnIAS);
	DDX_Control(pDX, IDC_IVS, m_btnIVS);
	DDX_Control(pDX, IDC_ATRIUM_SEPTUM, m_btnAtrium);
	DDX_Control(pDX, IDC_VENTRICULE_SEPTUM, m_btnVentricule);
	DDX_Control(pDX, IDC_BACHMANN_ALL, m_btnBachmann_All);
	DDX_Control(pDX, IDC_RBB_ALL, m_btnRBB_All);
	DDX_Control(pDX, IDC_LBB_ALL, m_btnLBB_All);
	DDX_Control(pDX, IDC_ATRIUM_ALL, m_btnAtrium_All);
	DDX_Control(pDX, IDC_VENTRICULE_ALL, m_btnVentricule_All);
	DDX_Control(pDX, IDC_SKETCH, m_Sketch);
	DDX_Control(pDX, IDC_EDIT, m_edtInput);
	DDX_Check(pDX, IDC_SINUS_NODE, m_bSinusNode);
	DDX_Check(pDX, IDC_EP_ON_RA, m_bPacemakerOnRA);
	DDX_Check(pDX, IDC_EP_ON_LA, m_bPacemakerOnLA);
	DDX_Check(pDX, IDC_EP_ON_RV, m_bPacemakerOnRV);
	DDX_Check(pDX, IDC_EP_ON_LV, m_bPacemakerOnLV);
	DDX_Check(pDX, IDC_JUNCTION, m_bJunction);
	DDX_Check(pDX, IDC_S_A, m_bS_A);
	DDX_Check(pDX, IDC_BACHMANN, m_bBachmann);
	DDX_Check(pDX, IDC_JAMES, m_bJames);
	DDX_Check(pDX, IDC_AV_FAST, m_bAVFast);
	DDX_Check(pDX, IDC_AV_SLOW, m_bAVSlow);
	DDX_Check(pDX, IDC_KENT_LEFT, m_bKentLeft);
	DDX_Check(pDX, IDC_KENT_RIGHT, m_bKentRight);
	DDX_Check(pDX, IDC_HIS_BUNDLE, m_bHisBundle);
	DDX_Check(pDX, IDC_RIGHT_BUNDLE_BRANCH, m_bRBB);
	DDX_Check(pDX, IDC_LEFT_BUNDLE_BRANCH, m_bLBB);
	DDX_Check(pDX, IDC_IAS, m_bIAS);
	DDX_Check(pDX, IDC_IVS, m_bIVS);
	DDX_Check(pDX, IDC_ATRIUM_SEPTUM, m_bAtrium);
	DDX_Check(pDX, IDC_VENTRICULE_SEPTUM, m_bVentricule);
	DDX_Check(pDX, IDC_BACHMANN_ALL, m_bBachmann_All);
	DDX_Check(pDX, IDC_RBB_ALL, m_bRBB_All);
	DDX_Check(pDX, IDC_LBB_ALL, m_bLBB_All);
	DDX_Check(pDX, IDC_ATRIUM_ALL, m_bAtrium_All);
	DDX_Check(pDX, IDC_VENTRICULE_ALL, m_bVentricule_All);
	DDX_Check(pDX, IDC_CHECK_ALL, m_bCheckAll);
}

BEGIN_MESSAGE_MAP(CAttrDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CAttrDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CAttrDlg::OnBnClickedCancel)
	ON_NOTIFY(NM_CLICK, IDC_ATTR_LIST, &CAttrDlg::OnClickAttrList)
	ON_BN_CLICKED(IDC_CHECK_ALL, &CAttrDlg::OnClickedCheckAll)
	ON_BN_CLICKED(IDC_ATRIUM_SEPTUM, &CAttrDlg::OnClickedCheckAtriumSeptum)
	ON_BN_CLICKED(IDC_VENTRICULE_SEPTUM, &CAttrDlg::OnClickedCheckVentriculeSeptum)
	ON_BN_CLICKED(IDC_SINUS_NODE, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_EP_ON_RA, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_EP_ON_LA, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_EP_ON_RV, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_EP_ON_LV, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_JUNCTION, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_S_A, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_BACHMANN, &CAttrDlg::OnClickedCheckBachmann)
	ON_BN_CLICKED(IDC_JAMES, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_AV_FAST, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_AV_SLOW, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_KENT_LEFT, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_KENT_RIGHT, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_HIS_BUNDLE, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_LEFT_BUNDLE_BRANCH, &CAttrDlg::OnClickedCheckLBB)
	ON_BN_CLICKED(IDC_RIGHT_BUNDLE_BRANCH, &CAttrDlg::OnClickedCheckRBB)
	ON_BN_CLICKED(IDC_IAS, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_IVS, &CAttrDlg::OnClickedOption)
	ON_BN_CLICKED(IDC_BACHMANN_ALL, &CAttrDlg::OnClickedCheckBachmann_All)
	ON_BN_CLICKED(IDC_LBB_ALL, &CAttrDlg::OnClickedCheckLBB_All)
	ON_BN_CLICKED(IDC_RBB_ALL, &CAttrDlg::OnClickedCheckRBB_All)
	ON_BN_CLICKED(IDC_ATRIUM_ALL, &CAttrDlg::OnClickedCheckAtrium_All)
	ON_BN_CLICKED(IDC_VENTRICULE_ALL, &CAttrDlg::OnClickedCheckVentricule_All)
	ON_EN_KILLFOCUS(IDC_EDIT, &CAttrDlg::OnKillfocusEdit)
	ON_WM_CLOSE()
END_MESSAGE_MAP()

// CAttrDlg 消息处理程序
BOOL CAttrDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	ReleaseAttrList();
    m_lstAttr.ModifyStyle(0, LVS_REPORT | LVS_SHOWSELALWAYS, 0);
	//设定一个用于存取column的结构lvc
	LVCOLUMN lvc;
	//设定存取模式
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	//用InSertColumn函数向窗口中插入柱
	for( int i=0 ; i<2; i++ )
	{
		lvc.iSubItem = i;
		lvc.pszText = _gszColumnLabel[i];
		lvc.cx = _gnColumnWidth[i];
		lvc.fmt = _gnColumnFmt[i];
        m_lstAttr.InsertColumn(i,&lvc);
	}
	//设定列表一行全部选中的特性和显示网格的属性
	m_lstAttr.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    //设置字体
	//CFont font;
	//font.CreateFont(18,0,0,0,0,FALSE,FALSE,0,
	//    HANGUL_CHARSET,
	//    OUT_DEFAULT_PRECIS,
	//    CLIP_DEFAULT_PRECIS,
	//    DEFAULT_QUALITY,
	//    DEFAULT_PITCH|FF_SWISS,"宋体");
	//list.SetFont(&font, TRUE);

    // 把Edit控件藏起来
	m_edtInput.ShowWindow(SW_HIDE);

	// 配置Sketch控件
	m_Sketch.SetSketchDC(m_pSketchDC, m_nSketchW, m_nSketchH);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CAttrDlg::OnClose()
{
	ReleaseAttrList();
	CDialogEx::OnClose();
}

void CAttrDlg::ReleaseAttrList()
{
	int i;
	for (i=0; i<m_nAttrListIndexMax; i++) {
		LIST_PARAM* lpp = (LIST_PARAM*)m_lstAttr.GetItemData(i);
		delete lpp;
	}
	m_lstAttr.DeleteAllItems();
	m_nAttrListIndexMax = 0;
}

void CAttrDlg::OnClickAttrList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	CPoint pt;
	GetCursorPos(&pt);
	m_lstAttr.ScreenToClient(&pt);

	// 得到选中的行数
    UINT uFlags = 0;
    int nIndex = m_lstAttr.HitTest( pt, &uFlags );
    if( nIndex < 0 || nIndex >= m_nAttrListIndexMax )
    {
        *pResult = 0;
        return;
    }

	// 编辑选中的行
	CRect RectList;
	CRect RectItem;
	m_lstAttr.GetWindowRect(&RectList);
	ScreenToClient(&RectList);
	m_lstAttr.GetItemRect(nIndex, &RectItem, LVIR_BOUNDS);

    // 根据边框等因素修正矩形的尺寸
	int nColumWidth = m_lstAttr.GetColumnWidth(0);
    int nScrollPos = m_lstAttr.GetScrollPos(SB_HORZ);
	RectItem.left = nColumWidth - nScrollPos;
	RectItem.OffsetRect(RectList.TopLeft());

	// 初始化编辑框的内容
    CString strSel = m_lstAttr.GetItemText(nIndex, 1);
	m_edtInput.SetWindowText(strSel);
    // 调整位置并显示
    RectItem.left += 4;
	RectItem.top  += 2;
	m_edtInput.MoveWindow(&RectItem, TRUE);
	m_edtInput.ShowWindow(SW_SHOW);
	m_bEditEnableState = TRUE;
	// （注：之所以添加这个标志是因为“失去焦点”有时会被调用多次）
	// 将选中的位置存入成员变量
	m_nEditSelectedIndex = nIndex;
    // 将Edit控件设置为焦点
    m_edtInput.SetFocus();
    m_edtInput.SetSel(255,0);

	*pResult = 0;
}

void CAttrDlg::OnKillfocusEdit()
{
	// m_bEditEnableState标志保证失去焦点处理只进行一次
	if( !m_bEditEnableState )
		return;

	// 读出编辑框内容
	CString strValue;
	m_edtInput.GetWindowText( strValue );

	// 将编辑框清空，设置为不显示状态
	m_edtInput.SetWindowText(_T(""));
	m_edtInput.ShowWindow(SW_HIDE);

	// 得到选中的行数
	int nIndex = m_nEditSelectedIndex;
	if( nIndex < 0 || nIndex >= m_nAttrListIndexMax )
    {
       	m_bEditEnableState = FALSE;
        return;
    }

    // 修改List显示的内容
	m_lstAttr.SetItemText( nIndex, 1, strValue );

    // 修改数据
	LIST_PARAM* lpp = (LIST_PARAM*)m_lstAttr.GetItemData(nIndex);
	switch ((int)lpp->nType) {
	case LIST_PARAM_TYPE_INT:
		*((int*)lpp->pData) = _ttoi(strValue);
		break;

	case LIST_PARAM_TYPE_FLOAT:
		*((float*)lpp->pData) = _ttof(strValue);
		break;

	case LIST_PARAM_TYPE_ATRIUM:
		*((int*)lpp->pData) = _ttoi(strValue);
		DuplicateAtrium();
		break;

	case LIST_PARAM_TYPE_VENTRICLE:
		*((int*)lpp->pData) = _ttoi(strValue);
		DuplicateVentricle();
		break;

	case LIST_PARAM_TYPE_BACHMANN:
		*((int*)lpp->pData) = _ttoi(strValue);
		DuplicateBachmann();
		break;

	case LIST_PARAM_TYPE_RBB:
		*((int*)lpp->pData) = _ttoi(strValue);
		DuplicateRBB();
		break;

	case LIST_PARAM_TYPE_LBB:
		*((int*)lpp->pData) = _ttoi(strValue);
		DuplicateLBB();
		break;

	default:
		switch (lpp->nType) {
		case LIST_PARAM_TYPE_INT:
			*((int*)lpp->pData) = _ttoi(strValue);
			break;
		case LIST_PARAM_TYPE_FLOAT:
			*((float*)lpp->pData) = (float)_ttof(strValue);
			break;
		}
		break;
	}

	// 清除m_bEditEnableState标志
	m_bEditEnableState = FALSE;

	// 更新缩略图（阻滞标记与参数有关）
	UpdateSketch();

	//CString info;
	//info.Format(_T("AttrDlg::OnKillfocusEdit: %d\n"), m_setup.aryRBB[0].constConductionTime);
	//OutputDebugString(info);
}

void CAttrDlg::DuplicateBachmann()
{
	int i;
	for (i=1; i<BACHMANN_UNIT_COUNT; i++) {
		memcpy(&(m_setup.aryBachmann[i]),
			&(m_setup.aryBachmann[0]),
			sizeof(CONDUCTION_UNIT));
	}
}

void CAttrDlg::DuplicateRBB()
{
	int i;
	for (i=1; i<RBB_UNIT_COUNT; i++) {
		memcpy(&(m_setup.aryRBB[i]),
			&(m_setup.aryRBB[0]),
			sizeof(CONDUCTION_UNIT));
	}
}

void CAttrDlg::DuplicateLBB()
{
	int i;
	for (i=1; i<LBB_UNIT_COUNT; i++) {
		memcpy(&(m_setup.aryLBB[i]),
			&(m_setup.aryLBB[0]),
			sizeof(CONDUCTION_UNIT));
	}
}

void CAttrDlg::DuplicateAtrium()
{
	int i;
	for (i=1; i<ATRIUM_UNIT_COUNT; i++) {
		memcpy(&(m_setup.aryAtrium[i]),
			&(m_setup.aryAtrium[0]),
			sizeof(CONDUCTION_UNIT));
	}
}

void CAttrDlg::DuplicateVentricle()
{
	int i;
	for (i=1; i<VENTRICLE_UNIT_COUNT; i++) {
		memcpy(&(m_setup.aryVentricle[i]),
			&(m_setup.aryVentricle[0]),
			sizeof(CONDUCTION_UNIT));
	}
}

void CAttrDlg::OnBnClickedOk()
{
	CDialogEx::OnOK();
}

void CAttrDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}

void CAttrDlg::OnClickedCheckAll()
{
	UpdateData();
	if (m_bCheckAll) {
		m_btnSinusNode.SetCheck(1);
		m_btnPacemakerOnRA.SetCheck(1);
		m_btnPacemakerOnLA.SetCheck(1);
		m_btnPacemakerOnRV.SetCheck(1);
		m_btnPacemakerOnLV.SetCheck(1);
		m_btnJunction.SetCheck(1);
		m_btnS_A.SetCheck(1);
		m_btnJames.SetCheck(1);
		m_vtnAVFast.SetCheck(1);
		m_btnAVSlow.SetCheck(1);
		m_btnKentLeft.SetCheck(1);
		m_btnKentRight.SetCheck(1);
		m_btnHisBundle.SetCheck(1);
		m_btnIAS.SetCheck(1);
		m_btnIVS.SetCheck(1);
		m_btnBachmann_All.SetCheck(1);
		m_btnRBB_All.SetCheck(1);
		m_btnLBB_All.SetCheck(1);
		m_btnAtrium_All.SetCheck(1);
		m_btnVentricule_All.SetCheck(1);
	}
	else {
		m_btnSinusNode.SetCheck(0);
		m_btnPacemakerOnRA.SetCheck(0);
		m_btnPacemakerOnLA.SetCheck(0);
		m_btnPacemakerOnRV.SetCheck(0);
		m_btnPacemakerOnLV.SetCheck(0);
		m_btnJunction.SetCheck(0);
		m_btnS_A.SetCheck(0);
		m_btnJames.SetCheck(0);
		m_vtnAVFast.SetCheck(0);
		m_btnAVSlow.SetCheck(0);
		m_btnKentLeft.SetCheck(0);
		m_btnKentRight.SetCheck(0);
		m_btnHisBundle.SetCheck(0);
		m_btnIAS.SetCheck(0);
		m_btnIVS.SetCheck(0);
		m_btnBachmann_All.SetCheck(0);
		m_btnRBB_All.SetCheck(0);
		m_btnLBB_All.SetCheck(0);
		m_btnAtrium_All.SetCheck(0);
		m_btnVentricule_All.SetCheck(0);
	}
	m_btnBachmann.SetCheck(0);
	m_btnRBB.SetCheck(0);
	m_btnLBB.SetCheck(0);
	m_btnAtrium.SetCheck(0);
	m_btnVentricule.SetCheck(0);

	UpdateData();
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckBachmann()
{
	UpdateData();
	if (m_bBachmann) {
		m_btnBachmann_All.SetCheck(0);
		m_bBachmann_All = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckBachmann_All()
{
	UpdateData();
	if (m_bBachmann_All) {
		m_btnBachmann.SetCheck(0);
		m_bBachmann = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckRBB()
{
	UpdateData();
	if (m_bRBB) {
		m_btnRBB_All.SetCheck(0);
		m_bRBB_All = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckRBB_All()
{
	UpdateData();
	if (m_bRBB_All) {
		m_btnRBB.SetCheck(0);
		m_bRBB = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckLBB()
{
	UpdateData();
	if (m_bLBB) {
		m_btnLBB_All.SetCheck(0);
		m_bLBB_All = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckLBB_All()
{
	UpdateData();
	if (m_bLBB_All) {
		m_btnLBB.SetCheck(0);
		m_bLBB = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckAtriumSeptum()
{
	UpdateData();
	if (m_bAtrium) {
		m_btnAtrium_All.SetCheck(0);
		m_bAtrium_All = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckAtrium_All()
{
	UpdateData();
	if (m_bAtrium_All) {
		m_btnAtrium.SetCheck(0);
		m_bAtrium = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckVentriculeSeptum()
{
	UpdateData();
	if (m_bVentricule) {
		m_btnVentricule_All.SetCheck(0);
		m_bVentricule_All = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedCheckVentricule_All()
{
	UpdateData();
	if (m_bVentricule_All) {
		m_btnVentricule.SetCheck(0);
		m_bVentricule = FALSE;
	}
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::OnClickedOption()
{
	UpdateData();
	UpdateList();
	UpdateSketch();
}

void CAttrDlg::UpdateSketch()
{
	if (NULL == theApp.m_pView) {
		return;
	}
	theApp.m_pView->DrawSketch(m_pSketchDC, &m_setup,
		m_bSinusNode,
		m_bPacemakerOnRA,
		m_bPacemakerOnLA,
		m_bPacemakerOnRV,
		m_bPacemakerOnLV,
		m_bJunction,
		m_bS_A,
		m_bBachmann | m_bBachmann_All,
		m_bJames,
		m_bAVFast,
		m_bAVSlow,
		m_bKentLeft,
		m_bKentRight,
		m_bHisBundle,
		m_bRBB | m_bRBB_All,
		m_bLBB | m_bLBB_All,
		m_bIAS,
		m_bIVS,
		m_bAtrium | m_bAtrium_All,
		m_bVentricule | m_bVentricule_All);
	m_Sketch.Invalidate(0);
}

#define LIST_ITEM_TEXT_MAX    (100)
void CAttrDlg::UpdateList()
{
	ReleaseAttrList();

	if (m_bSinusNode) {
		InsertSU(m_setup.suSinusNode, &m_nAttrListIndexMax);
	}

	if (m_bPacemakerOnRA) {
		InsertSU(m_setup.suEctopicPacemakerOnRA, &m_nAttrListIndexMax);
	}

	if (m_bPacemakerOnLA) {
		InsertSU(m_setup.suEctopicPacemakerOnLA, &m_nAttrListIndexMax);
	}

	if (m_bPacemakerOnRV) {
		InsertSU(m_setup.suEctopicPacemakerOnRV, &m_nAttrListIndexMax);
	}

	if (m_bPacemakerOnLV) {
		InsertSU(m_setup.suEctopicPacemakerOnLV, &m_nAttrListIndexMax);
	}

	if (m_bJunction) {
		InsertSU(m_setup.suJunction, &m_nAttrListIndexMax);
	}

	if (m_bS_A) {
		InsertCU(m_setup.cuS_A, &m_nAttrListIndexMax);
	}

	if (m_bJames) {
		InsertCU(m_setup.cuJames, &m_nAttrListIndexMax);
	}

	if (m_bAVFast) {
		InsertCU(m_setup.cuAVFast, &m_nAttrListIndexMax);
	}

	if (m_bAVSlow) {
		InsertCU(m_setup.cuAVSlow, &m_nAttrListIndexMax);
	}

	if (m_bKentLeft) {
		InsertCU(m_setup.cuKentLeft, &m_nAttrListIndexMax);
	}

	if (m_bKentRight) {
		InsertCU(m_setup.cuKentRight, &m_nAttrListIndexMax);
	}

	if (m_bHisBundle) {
		InsertCU(m_setup.cuHisBundle, &m_nAttrListIndexMax);
	}

	if (m_bIAS) {
		InsertCU(m_setup.cuIAS, &m_nAttrListIndexMax);
	}

	if (m_bIVS) {
		InsertCU(m_setup.cuIVS, &m_nAttrListIndexMax);
	}

	if (m_bBachmann_All) {
		InsertCU(m_setup.aryBachmann[0], &m_nAttrListIndexMax, CU_FLAG_BACHMANN);
	}

	if (m_bRBB_All) {
		InsertCU(m_setup.aryRBB[0], &m_nAttrListIndexMax, CU_FLAG_RBB);
	}

	if (m_bLBB_All) {
		InsertCU(m_setup.aryLBB[0], &m_nAttrListIndexMax, CU_FLAG_LBB);
	}

	if (m_bAtrium_All) {
		InsertCU(m_setup.aryAtrium[0], &m_nAttrListIndexMax, CU_FLAG_ATRIUM);
	}

	if (m_bVentricule_All) {
		InsertCU(m_setup.aryVentricle[0], &m_nAttrListIndexMax, CU_FLAG_VENTRICLE);
	}

	if (m_bBachmann) {
		int i;
		for (i=0; i<BACHMANN_UNIT_COUNT; i++) {
			InsertCU(m_setup.aryBachmann[i], &m_nAttrListIndexMax);
		}
	}

	if (m_bRBB) {
		int i;
		for (i=0; i<RBB_UNIT_COUNT; i++) {
			InsertCU(m_setup.aryRBB[i], &m_nAttrListIndexMax);
		}
	}

	if (m_bLBB) {
		int i;
		for (i=0; i<LBB_UNIT_COUNT; i++) {
			InsertCU(m_setup.aryLBB[i], &m_nAttrListIndexMax);
		}
	}

	if (m_bAtrium) {
		int i;
		for (i=0; i<ATRIUM_UNIT_COUNT; i++) {
			InsertCU(m_setup.aryAtrium[i], &m_nAttrListIndexMax);
		}
	}

	if (m_bVentricule) {
		int i;
		for (i=0; i<VENTRICLE_UNIT_COUNT; i++) {
			InsertCU(m_setup.aryVentricle[i], &m_nAttrListIndexMax);
		}
	}
}

void CAttrDlg::InsertSU(SPONTANEOUS_UNIT& su, int* pIndex)
{
	CString sLabel;
	CString sValue;
	LPARAM lparam;
	LIST_PARAM* lpp;

	sLabel = _T("是否完全阻滞");
	sValue.Format(_T("%d"), su.bOutputBlock ? 1 : 0);
	lpp = new LIST_PARAM;
	lpp->nType = LIST_PARAM_TYPE_INT;
	lpp->pData = (LPARAM)&su.bOutputBlock;
	InsertListItem((*pIndex) ++, su.name, sLabel, sValue, lpp);

	sLabel = _T("频率(次/分钟)");
	sValue.Format(_T("%.1f"), su.fBaseFreqency);
	lpp = new LIST_PARAM;
	lpp->nType = LIST_PARAM_TYPE_FLOAT;
	lpp->pData = (LPARAM)&su.fBaseFreqency;
	InsertListItem((*pIndex) ++, su.name, sLabel, sValue, lpp);

	sLabel = _T("频率方差(>10.0)");
	sValue.Format(_T("%.1f"), su.fGaussianVariance);
	lpp = new LIST_PARAM;
	lpp->nType = LIST_PARAM_TYPE_FLOAT;
	lpp->pData = (LPARAM)&su.fGaussianVariance;
	InsertListItem((*pIndex) ++, su.name, sLabel, sValue, lpp);

	sLabel = _T("被抑制时间(ms)");
	sValue.Format(_T("%d"), su.constSuppressedTimeDelay);
	lpp = new LIST_PARAM;
	lpp->nType = LIST_PARAM_TYPE_INT;
	lpp->pData = (LPARAM)&su.constSuppressedTimeDelay;
	InsertListItem((*pIndex) ++, su.name, sLabel, sValue, lpp);

	sLabel = _T("系统初始不应期(ms)");
	sValue.Format(_T("%d"), su.nInactiveTimeDelay);
	lpp = new LIST_PARAM;
	lpp->nType = LIST_PARAM_TYPE_INT;
	lpp->pData = (LPARAM)&su.nInactiveTimeDelay;
	InsertListItem((*pIndex) ++, su.name, sLabel, sValue, lpp);

	sLabel = _T("激活时间比例(例0.02)");
	sValue.Format(_T("%.2f"), su.ts.constActiveRatio);
	lpp = new LIST_PARAM;
	lpp->nType = LIST_PARAM_TYPE_FLOAT;
	lpp->pData = (LPARAM)&su.ts.constActiveRatio;
	InsertListItem((*pIndex) ++, su.name, sLabel, sValue, lpp);

	sLabel = _T("不应期时间比例(例0.25)");
	sValue.Format(_T("%.2f"), su.ts.constRefractoryRatio);
	lpp = new LIST_PARAM;
	lpp->nType = LIST_PARAM_TYPE_FLOAT;
	lpp->pData = (LPARAM)&su.ts.constRefractoryRatio;
	InsertListItem((*pIndex) ++, su.name, sLabel, sValue, lpp);
}

void CAttrDlg::InsertCU(CONDUCTION_UNIT& cu, int* pIndex, int nFlag)
{
	CString sLabel;
	CString sValue;
	LPARAM lparam;
	LIST_PARAM* lpp;

	int nParamType;
	switch (nFlag) {
	case CU_FLAG_NORMAL:	nParamType = LIST_PARAM_TYPE_INT;       break;
	case CU_FLAG_ATRIUM:	nParamType = LIST_PARAM_TYPE_ATRIUM;    break;
	case CU_FLAG_VENTRICLE: nParamType = LIST_PARAM_TYPE_VENTRICLE; break;
	case CU_FLAG_BACHMANN:	nParamType = LIST_PARAM_TYPE_BACHMANN;	break;
	case CU_FLAG_RBB:		nParamType = LIST_PARAM_TYPE_RBB;		break;
	case CU_FLAG_LBB:		nParamType = LIST_PARAM_TYPE_LBB;		break;
	}

	sLabel = _T("是否完全阻滞");
	sValue.Format(_T("%d"), cu.bCompleteBlock ? 1 : 0);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.bCompleteBlock;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);

	sLabel = _T("传导时间(ms)");
	sValue.Format(_T("%d"), cu.constConductionTime);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.constConductionTime;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);
	
	sLabel = _T("端1到端2传导阻滞");
	sValue.Format(_T("%d"), cu.b1to2Block);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.b1to2Block;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);

	sLabel = _T("端1激活(ms)");
	sValue.Format(_T("%d"), cu.ts1.constActivePeriod);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.ts1.constActivePeriod;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);

	sLabel = _T("端1不应期(ms)");
	sValue.Format(_T("%d"), cu.ts1.constRefractoryPeriod);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.ts1.constRefractoryPeriod;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);

	sLabel = _T("端2到端1传导阻滞");
	sValue.Format(_T("%d"), cu.b2to1Block);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.b2to1Block;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);

	sLabel = _T("端2激活(ms)");
	sValue.Format(_T("%d"), cu.ts2.constActivePeriod);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.ts2.constActivePeriod;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);

	sLabel = _T("端2不应期(ms)");
	sValue.Format(_T("%d"), cu.ts2.constRefractoryPeriod);
	lpp = new LIST_PARAM;
	lpp->nType = nParamType;
	lpp->pData = (LPARAM)&cu.ts2.constRefractoryPeriod;
	InsertListItem((*pIndex) ++, cu.name, sLabel, sValue, lpp);
}

void CAttrDlg::InsertListItem(int index, CString sName,
							  CString sLabel, CString sValue, LIST_PARAM* lparam)
{
	CString sNewLabel;

	LVITEM lvi;
	ZeroMemory(&lvi, sizeof(lvi));
	TCHAR pszText[LIST_ITEM_TEXT_MAX];
	lvi.cchTextMax = LIST_ITEM_TEXT_MAX;
	lvi.pszText = pszText;

	lvi.iItem = index;
	lvi.iSubItem = 0;
	lvi.mask = LVIF_TEXT | LVIF_PARAM;

	switch ((int)lparam->nType) {
	case LIST_PARAM_TYPE_ATRIUM:
		sNewLabel = _T("心房-");
		sNewLabel += sLabel;
		break;

	case LIST_PARAM_TYPE_VENTRICLE:
		sNewLabel = _T("心室-");
		sNewLabel += sLabel;
		break;

	case LIST_PARAM_TYPE_BACHMANN:
		sNewLabel = _T("Bachmann-");
		sNewLabel += sLabel;
		break;

	case LIST_PARAM_TYPE_RBB:
		sNewLabel = _T("右束支-");
		sNewLabel += sLabel;
		break;

	case LIST_PARAM_TYPE_LBB:
		sNewLabel = _T("左束支-");
		sNewLabel += sLabel;
		break;

	default:
		sNewLabel = sName;
		sNewLabel += _T("-");
		sNewLabel += sLabel;
		break;
	}
	wcsncpy(lvi.pszText, sNewLabel.GetBuffer(0), LIST_ITEM_TEXT_MAX);
	lvi.lParam = (LPARAM)lparam;
	m_lstAttr.InsertItem(&lvi);

	lvi.iSubItem = 1;
	lvi.mask = LVIF_TEXT;
	wcsncpy(lvi.pszText, sValue.GetBuffer(0), LIST_ITEM_TEXT_MAX);
	m_lstAttr.SetItem(&lvi);
}

