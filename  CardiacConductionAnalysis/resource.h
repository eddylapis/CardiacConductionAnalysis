//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CardiacConductionAnalysis.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_CCATYPE                     130
#define IDD_ATTR_DLG                    310
#define IDB_BITMAP1                     311
#define IDB_ATRIUM                      313
#define IDB_AV_FAST                     314
#define IDB_AV_SLOW                     315
#define IDB_BACHMANN                    316
#define IDB_BACKGROUND                  317
#define IDB_BITMAP7                     318
#define IDB_HIS_BUNDLE                  318
#define IDB_INTERATRIAL_SEPTUM          319
#define IDB_JAMES                       320
#define IDB_JUNCTION                    321
#define IDB_KENT_LEFT                   322
#define IDB_KENT_RIGHT                  323
#define IDB_LEFT_BUNDLE_BRANCH          324
#define IDB_PACEMAKER_ON_LA             325
#define IDB_PAC                         326
#define IDB_PACEMAKER_ON_LV             326
#define IDB_PACEMAKER_ON_RA             327
#define IDB_PACEMAKER_ON_RV             328
#define IDB_RIGHT_BUNDLE_BRANCH         329
#define IDB_S_A                         330
#define IDB_SINUE_NODE                  331
#define IDB_VENTRICLE                   332
#define IDB_HEART_MODEL                 334
#define IDB_ALL_IN_ONE                  336
#define IDB_IVS                         341
#define IDB_IAS                         342
#define IDC_ATTR_LIST                   1000
#define IDC_CHECK_ALL                   1001
#define IDC_CHECK_NOTHING               1001
#define IDC_SINUS_NODE                  1002
#define IDC_EP_ON_RA                    1003
#define IDC_EP_ON_LA                    1004
#define IDC_EP_ON_RV                    1005
#define IDC_EP_ON_LV                    1006
#define IDC_JUNCTION                    1007
#define IDC_S_A                         1008
#define IDC_BACHMANN                    1009
#define IDC_JAMES                       1010
#define IDC_AV_FAST                     1011
#define IDC_AV_SLOW                     1012
#define IDC_KENT_LEFT                   1013
#define IDC_KENT_RIGHT                  1014
#define IDC_HIS_BUNDLE                  1015
#define IDC_RIGHT_BUNDLE_BRANCH         1016
#define IDC_LEFT_BUNDLE_BRANCH          1017
#define IDC_ATRIUM_SEPTUM               1018
#define IDC_VENTRICULE_SEPTUM           1019
#define IDC_ILLUSTRATION                1020
#define IDC_INTERATRIAL_SEPTUM          1021
#define IDC_SKETCH                      1022
#define IDC_VENTRICULE_ALL              1023
#define IDC_EDIT                        1024
#define IDC_IAS                         1026
#define IDC_IVS                         1027
#define IDC_RBB_ALL                     1029
#define IDC_LBB_ALL                     1030
#define IDC_ATRIUM_ALL                  1031
#define IDC_BACHMANN_ALL                1032
#define ID_EDIT_ATTR                    32771
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_1MS_PER_PIXEL                32776
#define ID_5MS_PER_PIXEL                32777
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_EXPORT                       32782
#define ID_EXPORT_SEQUENCE              32783
#define ID_EX                           32784

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        343
#define _APS_NEXT_COMMAND_VALUE         32785
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
