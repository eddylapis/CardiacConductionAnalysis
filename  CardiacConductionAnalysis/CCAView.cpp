
// CCAView.cpp : CCAView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "CardiacConductionAnalysis.h"
#endif

#include "SmartDC.h"
#include "CCADoc.h"
#include "CCAView.h"
#include "MainFrm.h"
#include "AttrDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

static BOOL g_FLAG_test_uniform = FALSE;

static int _nViewWidth = 2000;
static int _nHeadlineW = 150;
static int _nBarWidth = _nViewWidth - _nHeadlineW;
static int _nViewHeight = 1000;
// CCAView

IMPLEMENT_DYNCREATE(CCAView, CScrollView)

BEGIN_MESSAGE_MAP(CCAView, CScrollView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CScrollView::OnFilePrintPreview)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEWHEEL()
	ON_COMMAND(ID_1MS_PER_PIXEL, &CCAView::On1msPerPixel)
	ON_COMMAND(ID_5MS_PER_PIXEL, &CCAView::On5msPerPixel)
	ON_COMMAND(ID_EDIT_ATTR, &CCAView::OnEditAttr)
	ON_COMMAND(ID_EXPORT_SEQUENCE, &CCAView::OnExportSequence)
END_MESSAGE_MAP()

// CCAView 构造/析构
CCAView::CCAView()
{
	m_b1msPerPixel = FALSE;//默认情况下是每5ms一个像素
	m_pMemDC = NULL;
	m_pBarChartDC = NULL;
	m_pLocationDC = NULL;
	m_pSketchImageDC = NULL;
	m_pBarChartBuffer = NULL;
	theApp.m_pView = this;
}

CCAView::~CCAView()
{
	delete m_pMemDC;
	delete m_pBarChartDC;
	delete m_pLocationDC;
	delete m_pSketchImageDC;
	delete m_pdcBackground;
	delete m_pdcSinusNode;
	delete m_pdcEctopicPacemakerOnRA;
	delete m_pdcEctopicPacemakerOnLA;
	delete m_pdcEctopicPacemakerOnRV;
	delete m_pdcEctopicPacemakerOnLV;
	delete m_pdcJunction;
	delete m_pdcS_A;
	delete m_pdcBachmann;
	delete m_pdcJames;
	delete m_pdcAVFast;
	delete m_pdcAVSlow;
	delete m_pdcKentLeft;
	delete m_pdcKentRight;
	delete m_pdcHisBundle;
	delete m_pdcRBB;
	delete m_pdcLBB;
	delete m_pdcIAS;
	delete m_pdcIVS;
	delete m_pdcAtrium;
	delete m_pdcVentricle;

	ReleaseBarChartBuffer();

	theApp.m_pView = NULL;
}

void CCAView::ReleaseBarChartBuffer()
{
	if (m_pBarChartBuffer) {
		
		int i;
		for (i=0; i<m_nBarChartRow; i++) {
			PIN_POS* p = m_pBarChartBuffer[i];
			delete [] p;
		}

		delete [] m_pBarChartBuffer;
		m_pBarChartBuffer = NULL;
		m_nBarChartRow = 0;
	}
}

BOOL CCAView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CScrollView::PreCreateWindow(cs);
}

void CCAView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	sizeTotal.cx = _nViewWidth;
	sizeTotal.cy = _nViewHeight;
	SetScrollSizes(MM_TEXT, sizeTotal);
}

// CCAView 绘制
static BOOL isFirst = TRUE;
void CCAView::OnDraw(CDC* pDC)
{
	CCADoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	
	if (isFirst) {
		isFirst = FALSE;

		CFont font;
		font.CreateFont(12, 0, 0, 0, 400,
			FALSE, FALSE, FALSE, 0, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_ROMAN, _T("宋体"));  

		CRect rc(0, 0, _nViewWidth, _nViewHeight);
		m_pMemDC = new CSmartDC(pDC, &rc);
		m_pBarChartDC = new CSmartDC(pDC, &rc);

		CBitmap cbmp;
		cbmp.LoadBitmap(IDB_ALL_IN_ONE);
		m_pLocationDC = new CSmartDC(pDC);
		m_pLocationDC->SelectObject(&cbmp);
		BITMAP bmp;
		cbmp.GetBitmap(&bmp);
		m_nLocationBmpW = bmp.bmWidth;
		m_nLocationBmpH = bmp.bmHeight;

		CRect rc_sketch(0, 0, m_nLocationBmpW, m_nLocationBmpH);
		m_pSketchImageDC = new CSmartDC(pDC, &rc_sketch);

		pDC->SelectObject(&font);
		m_pMemDC->SelectObject(&font);
		m_pBarChartDC->SelectObject(&font);
		font.DeleteObject();

		//初始化各个部位的图片DC
		InitUnitDC(pDC);

		//绘制柱状图
		RenewBarChart();
	}

	CBrush br_black(RGB(0, 0, 0));
	CBrush br_blue(RGB(0, 0, 255));

	m_pMemDC->BitBlt(0,0,_nViewWidth, _nViewHeight, m_pBarChartDC, 0, 0, SRCCOPY);

	CString sPos;
	int nPos = m_nMouseDown - _nHeadlineW;
	if (nPos > 0) {
		CRect rc2(m_nMouseDown, 0, m_nMouseDown+1, _nViewHeight);
		m_pMemDC->FillRect(&rc2, &br_blue);
		if (m_b1msPerPixel) {
			sPos.Format(_T("%d"), nPos);
		}
		else {
			sPos.Format(_T("%d"), nPos * 5);
		}
		m_pMemDC->TextOut(m_nMouseDown + 2, 4, sPos);
	}

	int nLeft = GetScrollPos(0);
	nPos = m_nMousePosX - _nHeadlineW;
	if (nPos > 0) {
		m_pMemDC->BitBlt(nLeft,0,_nHeadlineW,_nViewHeight, m_pBarChartDC,0,0, SRCCOPY);
		CRect rc3(m_nMousePosX, 0, m_nMousePosX+1, _nViewHeight);
		m_pMemDC->FillRect(&rc3, &br_black);
		CRect rc4(nLeft, m_nMousePosY, nLeft+_nViewWidth, m_nMousePosY+1);
		m_pMemDC->FillRect(&rc4, &br_black);

		//绘制位置图
		m_pMemDC->BitBlt(m_nMousePosX+1, m_nMousePosY+1, m_nLocationBmpW, m_nLocationBmpH,
			m_pLocationDC, 0, 0, SRCCOPY);
		//绘制PIN点
		int nBarChartIndex;
		if (m_b1msPerPixel) {
			nBarChartIndex = nPos;
		}
		else {
			nBarChartIndex = nPos * 5;
		}
		int i;
		for (i=0; i<m_nBarChartRow; i++) {
			PIN_POS* pp = m_pBarChartBuffer[i];
			if (pp) {
				PIN_POS& pos = pp[nBarChartIndex];
				CBrush br, *pOldBrush;
				switch (pos.state) {
				case TERMINAL_OUTPUT_ACTIVE:
					br.CreateSolidBrush(RGB(255,0,0));
					pOldBrush = m_pMemDC->SelectObject(&br);
					m_pMemDC->Ellipse(
						m_nMousePosX + pos.x - 4,
						m_nMousePosY + pos.y - 4,
						m_nMousePosX + pos.x + 7,
						m_nMousePosY + pos.y + 7);
					m_pMemDC->SelectObject(pOldBrush);
					break;

				case TERMINAL_OUTPUT_REFRACTORY:
					br.CreateSolidBrush(RGB(192,192,192));
					pOldBrush = m_pMemDC->SelectObject(&br);
					m_pMemDC->Ellipse(
						m_nMousePosX + pos.x - 4,
						m_nMousePosY + pos.y - 4,
						m_nMousePosX + pos.x + 7,
						m_nMousePosY + pos.y + 7);
					m_pMemDC->SelectObject(pOldBrush);
					break;
				}
			}
		}

		if (m_b1msPerPixel) {
			sPos.Format(_T("%d"), nPos);
		}
		else {
			sPos.Format(_T("%d"), nPos * 5);
		}
		m_pMemDC->TextOut(m_nMousePosX + 2, 4, sPos);
	}

	pDC->BitBlt(0, 0, _nViewWidth, _nViewHeight, m_pMemDC, 0, 0, SRCCOPY);

	//CBitmap cbmp;
	//cbmp.LoadBitmap(IDB_BACKGROUND);
	//m_pLocationDC->SelectObject(&cbmp);
	//pDC->BitBlt(0, 0, m_nLocationBmpW, m_nLocationBmpH,
	//		m_pdcJunction, 0, 0, SRCCOPY);
}

void CCAView::InitUnitDC(CDC* pDC)
{
	CBitmap* pbmp;

	m_pdcBackground = new CSmartDC(pDC);
	m_pdcSinusNode = new CSmartDC(pDC);
	m_pdcEctopicPacemakerOnRA = new CSmartDC(pDC);
	m_pdcEctopicPacemakerOnLA = new CSmartDC(pDC);
	m_pdcEctopicPacemakerOnRV = new CSmartDC(pDC);
	m_pdcEctopicPacemakerOnLV = new CSmartDC(pDC);
	m_pdcJunction = new CSmartDC(pDC);
	m_pdcS_A = new CSmartDC(pDC);
	m_pdcBachmann = new CSmartDC(pDC);
	m_pdcJames = new CSmartDC(pDC);
	m_pdcAVFast = new CSmartDC(pDC);
	m_pdcAVSlow = new CSmartDC(pDC);
	m_pdcKentLeft = new CSmartDC(pDC);
	m_pdcKentRight = new CSmartDC(pDC);
	m_pdcHisBundle = new CSmartDC(pDC);
	m_pdcRBB = new CSmartDC(pDC);
	m_pdcLBB = new CSmartDC(pDC);
	m_pdcIAS = new CSmartDC(pDC);
	m_pdcIVS = new CSmartDC(pDC);
	m_pdcAtrium = new CSmartDC(pDC);
	m_pdcVentricle = new CSmartDC(pDC);

	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_BACKGROUND);
	m_pdcBackground->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_SINUE_NODE);
	m_pdcSinusNode->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_PACEMAKER_ON_RA);
	m_pdcEctopicPacemakerOnRA->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_PACEMAKER_ON_LA);
	m_pdcEctopicPacemakerOnLA->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_PACEMAKER_ON_RV);
	m_pdcEctopicPacemakerOnRV->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_PACEMAKER_ON_LV);
	m_pdcEctopicPacemakerOnLV->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_JUNCTION);
	m_pdcJunction->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_S_A);
	m_pdcS_A->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_BACHMANN);
	m_pdcBachmann->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_JAMES);
	m_pdcJames->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_AV_FAST);
	m_pdcAVFast->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_AV_SLOW);
	m_pdcAVSlow->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_KENT_LEFT);
	m_pdcKentLeft->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_KENT_RIGHT);
	m_pdcKentRight->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_HIS_BUNDLE);
	m_pdcHisBundle->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_RIGHT_BUNDLE_BRANCH);
	m_pdcRBB->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_LEFT_BUNDLE_BRANCH);
	m_pdcLBB->SelectObject(pbmp);
	delete pbmp;

	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_IAS);
	m_pdcIAS->SelectObject(pbmp);
	delete pbmp;

	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_IVS);
	m_pdcIVS->SelectObject(pbmp);
	delete pbmp;

	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_ATRIUM);
	m_pdcAtrium->SelectObject(pbmp);
	delete pbmp;
	
	pbmp = new CBitmap;
	pbmp->LoadBitmap(IDB_VENTRICLE);
	m_pdcVentricle->SelectObject(pbmp);
	delete pbmp;
}

// CCAView 打印

BOOL CCAView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CCAView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CCAView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}

// CCAView 诊断
#ifdef _DEBUG
void CCAView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CCAView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CCADoc* CCAView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCADoc)));
	return (CCADoc*)m_pDocument;
}
#endif //_DEBUG

// 计算member在type中的位置
// #define offsetof(type, member)  (size_t)(&((type*)0)->member)
// 根据member的地址获取type的起始地址
// #define container_of(ptr, type, member) ({          \
//         const typeof(((type *)0)->member)*__mptr = (ptr);    \
//     (type *)((char *)__mptr - offsetof(type, member)); })
//返回值：
//TRUE:双向连接；  FALSE:单向连接
BOOL CCAView::GetTsPos(int* pTerminalOutput,
					   int* x, int* y,
					   int* myTerminal)
{
	static const int offset = (int)(&((TERMINAL_STATUS *)0)->nTerminalOutput);
	TERMINAL_STATUS* pTS = (TERMINAL_STATUS *)(((char *)pTerminalOutput) - offset);
	*x = pTS->constPinX;
	*y = pTS->constPinY;

	int i;
	for (i=0; i<UNIT_INPUT_MAX; i++) {
		if (0 == pTS->aryInputSource[i]) {
			return FALSE;
		}
		if (myTerminal == pTS->aryInputSource[i]) {
			//说明连接是双向的
			return TRUE;
		}
	}
	return FALSE;
}

void CCAView::DrawConnectionLine(int* pInput,
								 int x1, int y1,
								 int* myTerminal)
{
	//两种画笔
	//红色表示双向连线
	//绿色表示单向连线，开始端为一蓝点
	CPen penRed(PS_SOLID, 1, RGB(255, 0, 0));
	CPen penGreen(PS_SOLID, 1, RGB(0, 255, 0));
	CPen* pOldPen = NULL;

	int x2, y2;
	if (GetTsPos(pInput, &x2, &y2, myTerminal)) {
		pOldPen = m_pLocationDC->SelectObject(&penRed);
		m_pLocationDC->MoveTo(x1, y1);
		m_pLocationDC->LineTo(x2, y2);
	}
	else {
		CBrush br(RGB(0, 0, 255));
		CRect rc(x1-3, y1-3, x1+3, y1+3);
		m_pLocationDC->FillRect(&rc, &br);

		pOldPen = m_pLocationDC->SelectObject(&penGreen);
		m_pLocationDC->MoveTo(x1, y1);
		m_pLocationDC->LineTo(x2, y2);
	}

	if (pOldPen) {
		m_pLocationDC->SelectObject(pOldPen);
	}
}

void CCAView::InitSketchConnection(CCADoc* pDoc)
{
	if (m_pLocationDC) {
		delete m_pLocationDC;
	}
	CBitmap cbmp;
	cbmp.LoadBitmap(IDB_ALL_IN_ONE);
	m_pLocationDC = new CSmartDC(m_pMemDC);
	m_pLocationDC->SelectObject(&cbmp);

	CardiacConfig* pCfg = pDoc->GetData();
	int i, count;
	int x1, y1, j;

	SPONTANEOUS_UNIT** pSuGroup = pCfg->GetSuGroup(&count);
	for (i=0; i<count; i++) {
		SPONTANEOUS_UNIT* psu = pSuGroup[i];
		x1 = psu->ts.constPinX;
		y1 = psu->ts.constPinY;
		for (j=0; j<UNIFORM_NUM_COUNT; j++) {
			int* pInput = psu->ts.aryInputSource[i];

			if (0 != pInput) {
				DrawConnectionLine(pInput, x1, y1, &psu->ts.nTerminalOutput);
			}
			else {
				break;
			}
		}
	}

	CONDUCTION_UNIT** pCuGroup = pCfg->GetCuGroup(&count);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT* pcu = pCuGroup[i];

		x1 = pcu->ts1.constPinX;
		y1 = pcu->ts1.constPinY;
		for (j=0; j<UNIT_INPUT_MAX; j++) {
			int* pInput = pcu->ts1.aryInputSource[j];

			if (0 != pInput) {
				DrawConnectionLine(pInput, x1, y1, &pcu->ts1.nTerminalOutput);
			}
			else {
				break;
			}
		}

		x1 = pcu->ts2.constPinX;
		y1 = pcu->ts2.constPinY;
		for (j=0; j<UNIT_INPUT_MAX; j++) {
			int* pInput = pcu->ts2.aryInputSource[j];

			if (0 != pInput) {
				DrawConnectionLine(pInput, x1, y1, &pcu->ts2.nTerminalOutput);
			}
			else {
				break;
			}
		}
	}

	//绘制阻滞标记
	pSuGroup = pCfg->GetSuGroup(&count);
	for (i=0; i<count; i++) {
		SPONTANEOUS_UNIT* psu = pSuGroup[i];
		DrawSuBlockSign(m_pLocationDC, psu);
	}

	pCuGroup = pCfg->GetCuGroup(&count);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT* pcu = pCuGroup[i];
		DrawCuBlockSign(m_pLocationDC, pcu);
	}
}

void CCAView::DrawSuBlockSign(CDC* pdc, SPONTANEOUS_UNIT* psu)
{
	if (psu->bOutputBlock) {
		int x = psu->ts.constPinX;
		int y = psu->ts.constPinY;
		CPen penBlack(PS_SOLID, 3, RGB(0, 0, 0));
		CPen* pOldPen = pdc->SelectObject(&penBlack);
		pdc->MoveTo(x-4, y-3);
		pdc->LineTo(x+3, y+4);
		pdc->MoveTo(x+3, y-3);
		pdc->LineTo(x-4, y+4);
		pdc->SelectObject(pOldPen);
	}
}

void CCAView::DrawCuBlockSign(CDC* pdc, CONDUCTION_UNIT* pcu)
{
	if (pcu->bCompleteBlock) {
		CPen penBlack(PS_SOLID, 2, RGB(0, 0, 0));
		CPen* pOldPen = pdc->SelectObject(&penBlack);
		int x = pcu->ts1.constPinX;
		int y = pcu->ts1.constPinY;
		pdc->MoveTo(x-3, y-3);
		pdc->LineTo(x+3, y+3);
		pdc->MoveTo(x+3, y-3);
		pdc->LineTo(x-3, y+3);
		x = pcu->ts2.constPinX;
		y = pcu->ts2.constPinY;
		pdc->MoveTo(x-3, y-3);
		pdc->LineTo(x+3, y+3);
		pdc->MoveTo(x+3, y-3);
		pdc->LineTo(x-3, y+3);
		pdc->SelectObject(pOldPen);
	}
	else {
		if (pcu->b1to2Block) {
			CBrush br(RGB(0, 0, 0));
			int x = pcu->ts1.constPinX;
			int y = pcu->ts1.constPinY;
			CRect rc(x-3, y-2, x+2, y+3);
			pdc->FillRect(&rc, &br);
		}
		if (pcu->b2to1Block) {
			CBrush br(RGB(0, 0, 0));
			int x = pcu->ts2.constPinX;
			int y = pcu->ts2.constPinY;
			CRect rc(x-3, y-2, x+2, y+3);
			pdc->FillRect(&rc, &br);
		}
	}
}

void CCAView::RenewBarChart()
{
	CCADoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	BeginWaitCursor();
	InitSketchConnection(pDoc);
	DrawAll(pDoc, *m_pBarChartDC);
	EndWaitCursor();
}

// 绘图
void CCAView::DrawAll(CCADoc* pDoc, CDC& dc)
{
	this->DrawRuler(dc);
	this->DrawHeadline(pDoc, dc);
	this->DrawActiveBar(pDoc, dc);
}

// 绘制背景及标尺
static const int _nRulerTop = 10;
static const int _nRulerHeight = 20;
static const int _nSmallScaleH = 5;
static const int _nMiddleScaleH = 10;
static const int _nSecondScaleH = 15;
void CCAView::DrawRuler(CDC& dc)
{
	int i;
	int l = _nHeadlineW;
	int t = _nRulerTop - _nRulerHeight;
	int r = _nViewWidth;
	int b = _nRulerTop + _nRulerHeight - 1;

	dc.BitBlt(l, t, r-l, b-t, &dc, 0, 0, WHITENESS);
	// 绘制基线
	dc.MoveTo(l, b);
	dc.LineTo(r, b);

	if (m_b1msPerPixel) {
		// 绘制1ms刻度
		for (i=0; i<_nBarWidth; i++) {
			if (i % 100 == 0) {

				CString sScale;
				sScale.Format(_T("%d"), i);
				dc.TextOut(l+i+5, _nRulerTop, sScale);

				if (i % 1000 == 0) {
					// 绘制大格
					dc.MoveTo(l+i, b-_nSecondScaleH);
					dc.LineTo(l+i, b);
				}
				else if (i % 500 == 0) {
					// 绘制中格
					dc.MoveTo(l+i, b-_nMiddleScaleH);
					dc.LineTo(l+i, b);
				}
				else {
					// 绘制小格
					dc.MoveTo(l+i, b-_nSmallScaleH);
					dc.LineTo(l+i, b);
				}
			}
		}
	}
	else {
		// 绘制10ms刻度
		for (i=0; i<_nBarWidth; i++) {
			if (i % 20 == 0) {

				if (i % 200 == 0) {
					// 绘制大格
					dc.MoveTo(l+i, b-_nSecondScaleH);
					dc.LineTo(l+i, b);

					CString sScale;
					sScale.Format(_T("%d"), i * 5);
					dc.TextOut(l+i+5, _nRulerTop, sScale);
				}
				else if (i % 100 == 0) {
					// 绘制中格
					dc.MoveTo(l+i, b-_nMiddleScaleH);
					dc.LineTo(l+i, b);
				}
				else {
					// 绘制小格
					dc.MoveTo(l+i, b-_nSmallScaleH);
					dc.LineTo(l+i, b);
				}
			}
		}
	}
}

// 绘制左侧标题
void CCAView::DrawHeadline(CCADoc* pDoc, CDC& dc)
{
	CardiacConfig* pCfg = pDoc->GetData();
	//测试一下随机分布的情况
	if (g_FLAG_test_uniform) {
		pCfg->InitUniform();
	}

	int i, count;
	const int x = 10;
	const int su_step = 14;
	const int cu_step = 18;
	int y = _nRulerTop + _nRulerHeight;

	dc.BitBlt(0,0,_nHeadlineW,_nViewHeight,&dc,0,0,WHITENESS);

	CBrush br_red(RGB(255, 0, 0));
	SPONTANEOUS_UNIT** pSuGroup = pCfg->GetSuGroup(&count);
	for (i=0; i<count; i++) {
		SPONTANEOUS_UNIT* psu = pSuGroup[i];
		if (psu->bOutputBlock) {
			continue;
		}
		dc.TextOut(x, y, psu->name);

		//测试一下随机数的分布情况
		if (g_FLAG_test_uniform) {
			dc.TextOut(_nViewWidth - _nBarWidth, y, CardiacConfig::GetUniString(psu));
		}
		y += su_step;
	}
	y += cu_step - su_step;

	CBrush br_green(RGB(0, 192, 0));
	CONDUCTION_UNIT** pCuGroup = pCfg->GetCuGroup(&count);
	for (i=0; i<count; i++) {
		CONDUCTION_UNIT* pcu = pCuGroup[i];
		if (pcu->bCompleteBlock) {
			continue;
		}
		dc.TextOut(x, y, pcu->name);
		y += cu_step;
	}
}

// 绘制节点状态条
void CCAView::DrawActiveBar(CCADoc* pDoc, CDC& dc)
{
	if (g_FLAG_test_uniform) {
		return;
	}

	CardiacConfig* pCfg = pDoc->GetData();
	CardiacConfig config(pCfg);

	int i, su_count, cu_count;
	const int x = 10;
	const int su_step = 14;
	const int cu_step = 18;
	
	int click_1ms;
	CBrush br_red(RGB(255, 0, 0));
	CBrush br_green(RGB(0, 192, 0));
	CBrush br_blue(RGB(64, 128, 255));
	CBrush br_gray(RGB(192, 192, 192));
	SPONTANEOUS_UNIT** pSuGroup = config.GetSuGroup(&su_count);
	CONDUCTION_UNIT** pCuGroup = config.GetCuGroup(&cu_count);

	int nDataLen;
	if (m_b1msPerPixel) {
		nDataLen = _nBarWidth;
	}
	else {
		nDataLen = _nBarWidth * 5;
	}

	//创建BarChartBuffer
	ReleaseBarChartBuffer();
	m_nBarChartRow = 0;
	for (i=0; i<su_count; i++) {
		SPONTANEOUS_UNIT* psu = pSuGroup[i];
		if (! psu->bOutputBlock) {
			m_nBarChartRow ++;
		}
	}
	for (i=0; i<cu_count; i++) {
		CONDUCTION_UNIT* pcu = pCuGroup[i];
		if (! pcu->bCompleteBlock) {
			m_nBarChartRow += 2;//每个传导单元有2个端点
		}
	}
	
	m_pBarChartBuffer = new PIN_POS*[m_nBarChartRow];
	for (i=0; i<m_nBarChartRow; i++) {
		m_pBarChartBuffer[i] = new PIN_POS[nDataLen];
		ZeroMemory(m_pBarChartBuffer[i], sizeof(PIN_POS)*nDataLen);
	}

	dc.BitBlt(_nViewWidth - _nBarWidth, _nRulerTop + _nRulerHeight,
		_nBarWidth, _nViewHeight - _nRulerHeight,
		&dc, 0, 0, WHITENESS);

	for (click_1ms=0; click_1ms<nDataLen; click_1ms++) {

		//CString info;
		//info.Format(_T("click_1ms: %d\n"), click_1ms);
		//OutputDebugString(info);

		config.Click_1ms();
		int nBarCharBufferIndex = 0;

		int y = _nRulerTop + _nRulerHeight;
		for (i=0; i<su_count; i++) {
			SPONTANEOUS_UNIT* psu = pSuGroup[i];
			if (psu->bOutputBlock) {
				continue;
			}

			int rc_l;
			if (m_b1msPerPixel) {
				rc_l = _nViewWidth - _nBarWidth + click_1ms;
			}
			else {
				rc_l = _nViewWidth - _nBarWidth + click_1ms / 5;
			}
			
			//记下事件发生的位置
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].x = psu->ts.constPinX;
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].y = psu->ts.constPinY;
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_INACTIVE;
			if (psu->ts.nTerminalOutput == TERMINAL_OUTPUT_ACTIVE) {
				CRect rc(rc_l, y+4, rc_l+1, y+su_step-4);
				dc.FillRect(&rc, &br_red);
				m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_ACTIVE;
			}
			if (psu->ts.nTerminalOutput == TERMINAL_OUTPUT_REFRACTORY) {
				CRect rc(rc_l, y+4, rc_l+1, y+su_step-4);
				dc.FillRect(&rc, &br_gray);
				m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_REFRACTORY;
			}
			nBarCharBufferIndex ++;

			y += su_step;
		}
		y += cu_step - su_step;

		for (i=0; i<cu_count; i++) {
			CONDUCTION_UNIT* pcu = pCuGroup[i];
			if (pcu->bCompleteBlock) {
				continue;
			}
		
			int rc_l;
			if (m_b1msPerPixel) {
				rc_l = _nViewWidth - _nBarWidth + click_1ms;
			}
			else {
				rc_l = _nViewWidth - _nBarWidth + click_1ms / 5;
			}

			//记下事件发生的位置
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].x = pcu->ts1.constPinX;
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].y = pcu->ts1.constPinY;
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_INACTIVE;
			if (pcu->ts1.nTerminalOutput == TERMINAL_OUTPUT_ACTIVE) {
				CRect rc(rc_l, y+1, rc_l +1, y + su_step / 2 - 1);
				dc.FillRect(&rc, &br_green);
				m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_ACTIVE;
			}
			if (pcu->ts1.nTerminalOutput == TERMINAL_OUTPUT_REFRACTORY) {
				CRect rc(rc_l, y+1, rc_l +1, y + su_step / 2 - 1);
				dc.FillRect(&rc, &br_gray);
				m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_REFRACTORY;
			}
			nBarCharBufferIndex ++;

			//记下事件发生的位置
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].x = pcu->ts2.constPinX;
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].y = pcu->ts2.constPinY;
			m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_INACTIVE;
			if (pcu->ts2.nTerminalOutput == TERMINAL_OUTPUT_ACTIVE) {
				CRect rc(rc_l, y + su_step / 2 + 1, rc_l + 1, y + su_step -1);
				dc.FillRect(&rc, &br_blue);
				m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_ACTIVE;
			}
			if (pcu->ts2.nTerminalOutput == TERMINAL_OUTPUT_REFRACTORY) {
				CRect rc(rc_l, y + su_step / 2 + 1, rc_l + 1, y + su_step -1);
				dc.FillRect(&rc, &br_gray);
				m_pBarChartBuffer[nBarCharBufferIndex][click_1ms].state = TERMINAL_OUTPUT_REFRACTORY;
			}
			nBarCharBufferIndex ++;

			y += cu_step;
		}
	}
}

void CCAView::DrawSketch(CDC* pdc, CARDIAC_MODEL* pModel,
		BOOL bSinusNode,
		BOOL bPacemakerOnRA,
		BOOL bPacemakerOnLA,
		BOOL bPacemakerOnRV,
		BOOL bPacemakerOnLV,
		BOOL bJunction,
		BOOL bS_A,
		BOOL bBachmann,
		BOOL bJames,
		BOOL bAVFast,
		BOOL bAVSlow,
		BOOL bKentLeft,
		BOOL bKentRight,
		BOOL bHisBundle,
		BOOL bRBB,
		BOOL bLBB,
		BOOL bIAS,
		BOOL bIVS,
		BOOL bAtrium,
		BOOL bVentricule)
{
	pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcBackground, 0,0, SRCCOPY);

	if (bSinusNode) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcSinusNode, 0,0, SRCAND);
		DrawSuBlockSign(pdc, &pModel->suSinusNode);
	}
	if (bPacemakerOnRA) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcEctopicPacemakerOnRA, 0,0, SRCAND);
		DrawSuBlockSign(pdc, &pModel->suEctopicPacemakerOnRA);
	}
	if (bPacemakerOnLA) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcEctopicPacemakerOnLA, 0,0, SRCAND);
		DrawSuBlockSign(pdc, &pModel->suEctopicPacemakerOnLA);
	}
	if (bPacemakerOnRV) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcEctopicPacemakerOnRV, 0,0, SRCAND);
		DrawSuBlockSign(pdc, &pModel->suEctopicPacemakerOnRV);
	}
	if (bPacemakerOnLV) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcEctopicPacemakerOnLV, 0,0, SRCAND);
		DrawSuBlockSign(pdc, &pModel->suEctopicPacemakerOnLV);
	}
	if (bJunction) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcJunction, 0,0, SRCAND);
		DrawSuBlockSign(pdc, &pModel->suJunction);
	}
	if (bS_A) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcS_A, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuS_A);
	}
	if (bBachmann) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcBachmann, 0,0, SRCAND);
		int i;
		for (i=0; i<BACHMANN_UNIT_COUNT; i++) {
			DrawCuBlockSign(pdc, &pModel->aryBachmann[i]);
		}
	}
	if (bJames) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcJames, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuJames);
	}
	if (bAVFast) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcAVFast, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuAVFast);
	}
	if (bAVSlow) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcAVSlow, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuAVSlow);
	}
	if (bKentLeft) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcKentLeft, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuKentLeft);
	}
	if (bKentRight) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcKentRight, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuKentRight);
	}
	if (bHisBundle) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcHisBundle, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuHisBundle);
	}
	if (bRBB) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcRBB, 0,0, SRCAND);
		int i;
		for (i=0; i<RBB_UNIT_COUNT; i++) {
			DrawCuBlockSign(pdc, &pModel->aryRBB[i]);
		}
	}
	if (bLBB) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcLBB, 0,0, SRCAND);
		int i;
		for (i=0; i<LBB_UNIT_COUNT; i++) {
			DrawCuBlockSign(pdc, &pModel->aryLBB[i]);
		}
	}
	if (bIAS) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcIAS, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuIAS);
	}
	if (bIVS) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcIVS, 0,0, SRCAND);
		DrawCuBlockSign(pdc, &pModel->cuIVS);
	}
	if (bAtrium) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcAtrium, 0,0, SRCAND);
		int i;
		for (i=0; i<ATRIUM_UNIT_COUNT; i++) {
			DrawCuBlockSign(pdc, &pModel->aryAtrium[i]);
		}
	}
	if (bVentricule) {
		pdc->BitBlt(0,0,m_nLocationBmpW,m_nLocationBmpH, m_pdcVentricle, 0,0, SRCAND);
		int i;
		for (i=0; i<VENTRICLE_UNIT_COUNT; i++) {
			DrawCuBlockSign(pdc, &pModel->aryVentricle[i]);
		}
	}
}

void CCAView::OnMouseMove(UINT nFlags, CPoint point)
{
	m_nMousePosX = point.x +  GetScrollPos(0);
	m_nMousePosY = point.y + GetScrollPos(1);
	CScrollView::OnMouseMove(nFlags, point);
	Invalidate(0);
}

void CCAView::OnLButtonDown(UINT nFlags, CPoint point)
{
	int pos = GetScrollPos(0);
	m_nMouseDown = point.x + pos;
	CScrollView::OnLButtonDown(nFlags, point);
	Invalidate(0);
}

BOOL CCAView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	//if(zDelta < 0) {
	//	OnHScroll(SB_LINERIGHT, GetScrollPos(SB_VERT),  GetScrollBarCtrl(SB_VERT));
	//}
	//else if (zDelta > 0) {
	//	OnHScroll(SB_LINELEFT, GetScrollPos(SB_VERT),  GetScrollBarCtrl(SB_VERT));
	//}
	//Invalidate(0);
	//return TRUE;

	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}

void CCAView::On1msPerPixel()
{
	if (m_b1msPerPixel) {
		return;
	}

	m_b1msPerPixel = TRUE;
	RenewBarChart();
	Invalidate(0);
}

void CCAView::On5msPerPixel()
{
	if (! m_b1msPerPixel) {
		return;
	}

	m_b1msPerPixel = FALSE;
	RenewBarChart();
	Invalidate(0);
}

void CCAView::OnEditAttr()
{
	CCADoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	m_pSketchImageDC->BitBlt(0, 0, m_nLocationBmpW, m_nLocationBmpH,
		m_pdcBackground, 0, 0, SRCCOPY);
	CAttrDlg dlg(this, pDoc->GetData(), 
		m_pSketchImageDC, m_nLocationBmpW, m_nLocationBmpH);
	if (IDOK == dlg.DoModal()) {
		pDoc->GetData()->CopyFrom(dlg.GetData());
		pDoc->SetModifiedFlag();
		RenewBarChart();

		//CString info;
		//info.Format(_T("CCAView::OnEditAttr: %d\n"),
		//	pDoc->GetData()->GetModel()->aryRBB[0].constConductionTime);
		//OutputDebugString(info);
	}
}

static TCHAR l_szTxtFileFilter[] = _T("TXT Files (*.txt)|*.txt||");
void CCAView::OnExportSequence()
{
	CCADoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	CardiacConfig* pCfg = pDoc->GetData();

	CFileDialog dlg(FALSE, _T("txt"), NULL, OFN_HIDEREADONLY, l_szTxtFileFilter, this);
	dlg.m_ofn.lpstrTitle = _T("导出当前时序数据");
	if (IDOK != dlg.DoModal()) {
		return;
	}
	CString sFileName = dlg.GetPathName();

	//CString sDate = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	////文件名的格式是：(床号:文件号)年-月-日:时:分:秒.rcd
	//CString sFileName;
	//sFileName.Format(_T("%04d%02d%02d-%02d%02d%02d(%02d-%d).rcd"),
	//	tm.GetYear(), tm.GetMonth(), tm.GetDay(), tm.GetHour(), tm.GetMinute(), tm.GetSecond(),
	//	m_nIndex + 1, m_nFileCount + 1);
	//CString sFullName;
	//sFullName.Format(_T("%s%s/%s"), theApp.GetDataRoot(), RECORD_DIR, sFileName);

	//写入文件
	CFile file(sFileName, CFile::modeReadWrite | CFile::modeCreate | CFile::typeBinary);
	BYTE unicode_head[2];
	unicode_head[0] = 0xFF;
	unicode_head[1] = 0xFE;
	file.Write(unicode_head, 2);

	//写index部分
	int index = 0;
	int i, count;
	SPONTANEOUS_UNIT** pSuGroup = pCfg->GetSuGroup(&count);
	for (i = 0; i < count; i++) {
		SPONTANEOUS_UNIT* psu = pSuGroup[i];
		CString item_index;
		item_index.Format(_T("%d:\t"), index++);
		item_index += psu->name;
		if (psu->bOutputBlock) {
			item_index += _T("(blocked)");
		}
		item_index += _T("\r\n");
		file.Write(item_index.GetBuffer(0), sizeof(TCHAR)*item_index.GetLength());
	}
	CONDUCTION_UNIT** pCuGroup = pCfg->GetCuGroup(&count);
	for (i = 0; i<count; i++) {
		CONDUCTION_UNIT* pcu = pCuGroup[i];
		CString item_index;
		item_index.Format(_T("%d:\t"), index++);
		item_index += pcu->name;
		if (pcu->bCompleteBlock ||
			(pcu->b1to2Block && pcu->b2to1Block)) {
			item_index += _T("(blocked)");
		}
		item_index += _T("\r\n");
		file.Write(item_index.GetBuffer(0), sizeof(TCHAR)*item_index.GetLength());
	}

	//写入数据部分
	TCHAR separator[] = _T("----------------------------------------------------\r\n");
	file.Write(separator, sizeof(separator)-sizeof(TCHAR));//去掉尾部的结束符

	int click_1ms;
	int nDataLen;
	if (m_b1msPerPixel) {
		nDataLen = _nBarWidth;
	}
	else {
		nDataLen = _nBarWidth * 5;
	}

	for (i = 0; i<m_nBarChartRow; i++) {
		PIN_POS* pp = m_pBarChartBuffer[i];
		if (pp) {

			CString item_data;
			item_data.Format(_T("%d:\t"), i);
			for (click_1ms = 0; click_1ms < nDataLen; click_1ms++) {
				PIN_POS& pos = pp[click_1ms];

				switch (pos.state) {
				case TERMINAL_OUTPUT_ACTIVE:
					item_data += _T("1");
					break;

				case TERMINAL_OUTPUT_REFRACTORY:
					item_data += _T("2");
					break;

				default:
					item_data += _T("0");
					break;
				}
			}
			item_data += _T("\r\n");
			file.Write(item_data.GetBuffer(0), sizeof(TCHAR)*item_data.GetLength());
		}
	}

	TCHAR end_flag[] = _T("----------------------------------------------------\r\nEND");
	file.Write(end_flag, sizeof(end_flag) - sizeof(TCHAR));//去掉尾部的结束符

	file.Flush();
	file.Close();

	MessageBox(_T("OK，导出成功！"));
}
