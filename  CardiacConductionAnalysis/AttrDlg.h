#pragma once
#include "afxwin.h"
#include "afxcmn.h"

// CAttrDlg 对话框
#include "SketchImage.h"

#define LIST_PARAM_TYPE_INT        (1)
#define LIST_PARAM_TYPE_FLOAT      (2)
#define LIST_PARAM_TYPE_ATRIUM     (3)
#define LIST_PARAM_TYPE_VENTRICLE  (4)
#define LIST_PARAM_TYPE_BACHMANN   (5)
#define LIST_PARAM_TYPE_RBB        (6)
#define LIST_PARAM_TYPE_LBB        (7)

#define CU_FLAG_NORMAL             (0)
#define CU_FLAG_ATRIUM             (1)
#define CU_FLAG_VENTRICLE          (2)
#define CU_FLAG_BACHMANN           (3)
#define CU_FLAG_RBB                (4)
#define CU_FLAG_LBB                (5)

typedef struct __tag_LIST_PARAM
{
	int    nType;
	LPARAM pData;
} LIST_PARAM;

class CAttrDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CAttrDlg)

private:
	CARDIAC_MODEL m_setup;
	CSmartDC* m_pSketchDC;
	int m_nSketchW;
	int m_nSketchH;

    BOOL m_bEditEnableState;
    int  m_nEditSelectedIndex;
    int  m_nAttrListIndexMax;

	void UpdateList();
	void UpdateSketch();
	void ReleaseAttrList();
	void DuplicateBachmann();
	void DuplicateRBB();
	void DuplicateLBB();
	void DuplicateAtrium();
	void DuplicateVentricle();

	void InsertSU(SPONTANEOUS_UNIT& su, int* pIndex);
	void InsertCU(CONDUCTION_UNIT& cu, int* pIndex, int nFlag = 0);
	void InsertListItem(int index, CString sName,
		CString sLabel, CString sValue, LIST_PARAM* lparam);

public:
	CAttrDlg(CWnd* pParent, CardiacConfig* pConfig,
		CSmartDC* pSketchDC, int nSketchW, int nSketchH);
	virtual ~CAttrDlg();
	CARDIAC_MODEL* GetData() { return &m_setup; }

// 对话框数据
	enum { IDD = IDD_ATTR_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	CEdit m_edtInput;
	CListCtrl m_lstAttr;
	CButton m_btnCheckAll;
	CButton m_btnSinusNode;
	CButton m_btnPacemakerOnRA;
	CButton m_btnPacemakerOnLA;
	CButton m_btnPacemakerOnRV;
	CButton m_btnPacemakerOnLV;
	CButton m_btnJunction;
	CButton m_btnS_A;
	CButton m_btnBachmann;
	CButton m_btnJames;
	CButton m_vtnAVFast;
	CButton m_btnAVSlow;
	CButton m_btnKentLeft;
	CButton m_btnKentRight;
	CButton m_btnHisBundle;
	CButton m_btnRBB;
	CButton m_btnLBB;
	CButton m_btnIAS;
	CButton m_btnIVS;
	CButton m_btnAtrium;
	CButton m_btnVentricule;
	CButton m_btnBachmann_All;
	CButton m_btnRBB_All;
	CButton m_btnLBB_All;
	CButton m_btnAtrium_All;
	CButton m_btnVentricule_All;
	SketchImage m_Sketch;
	BOOL m_bCheckAll;
	BOOL m_bSinusNode;
	BOOL m_bPacemakerOnRA;
	BOOL m_bPacemakerOnLA;
	BOOL m_bPacemakerOnRV;
	BOOL m_bPacemakerOnLV;
	BOOL m_bJunction;
	BOOL m_bS_A;
	BOOL m_bBachmann;
	BOOL m_bJames;
	BOOL m_bAVFast;
	BOOL m_bAVSlow;
	BOOL m_bKentLeft;
	BOOL m_bKentRight;
	BOOL m_bHisBundle;
	BOOL m_bRBB;
	BOOL m_bLBB;
	BOOL m_bIAS;
	BOOL m_bIVS;
	BOOL m_bAtrium;
	BOOL m_bVentricule;
	BOOL m_bBachmann_All;
	BOOL m_bRBB_All;
	BOOL m_bLBB_All;
	BOOL m_bAtrium_All;
	BOOL m_bVentricule_All;
	afx_msg void OnClickAttrList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnClose();
	afx_msg void OnClickedCheckAll();
	afx_msg void OnClickedCheckBachmann();
	afx_msg void OnClickedCheckBachmann_All();
	afx_msg void OnClickedCheckRBB();
	afx_msg void OnClickedCheckRBB_All();
	afx_msg void OnClickedCheckLBB();
	afx_msg void OnClickedCheckLBB_All();
	afx_msg void OnClickedCheckAtriumSeptum();
	afx_msg void OnClickedCheckAtrium_All();
	afx_msg void OnClickedCheckVentriculeSeptum();
	afx_msg void OnClickedCheckVentricule_All();
	afx_msg void OnClickedOption();
	afx_msg void OnKillfocusEdit();
};
